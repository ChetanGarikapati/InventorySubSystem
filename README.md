# InventorySubSystem

## Overview
This application is an Inventory Management System written in java 8 and can be plugged into any application through REST Services and deployed in servlet container like  Tomcat.

## Implementation Technologies
* Java 8 (Open-JDK)
* Spring FrameWork 5.1.1.
* JPA 2.1 (Hibernate)
* Model Mapper 2.3
* DataBase MySQL 5.7
* SLF4J with LogBack.
* JUNIT-5 (Jupiter)
* Mockito 2.23

>NOTE: *Application is deployed through Maven and tested using Apache Tomcat.*

## Setup
The application will automatically create the Db-Schema however a database by name **Inventory** must be present if going with default name.
* All the database related changes can be made in the file **JPAProperties.properties** located in the **META-INF/** folder.
* All the REST Service are self explanatory and can be easily used detailed info will be provided in *User Guide*.

## Functionality

The following things are to be noted before going through available operations

1. The system automatically generates ProductId when a new product is loaded to DB.
2. The system also generates WarehouseId when a new Fulfillment center is created.
3. An AddressId will also be generated corresponding to each warehouse this can also be updated.
4. Each product is associated with a seller with SellerId in order to support DropShip. 
5. Inventory allocation is done based on sectors. A sector can be created based on city,state or region (generic geo-location as required by the user).
6. If product is available in multiple sectors SectorPriority can be provided to resolve ambiguity or this can be provided based on optimal distance to delivery region from warehouses. So resolve sectors optimally. By default all sectors have same priority.
7. If multiple warehouses are available in a given sector then allocation can be done based on warehouse priority for that sector. By default all warehouses are given same priority.

```sh
NOTE: 
1. Sector Resolution precedence is city > state > region i.e if city is passed in hold request then sector is resolved based on city, if sector should be resolved by "region" then donot pass city and state in request, see JAVADOCS for more info.  
```

### Available Operations 

```sh
1. Check for Product Inventory by ProductId.
2. Hold/Reserve Inventory by ProductId.
3. Hold/Reserve multiple Products with one request.
4. Create warehouses and provide address.
5. Create sectors and provide priorities for sectors.
6. Manage warehouse priority for each warehouse in a sector.
7. Manage/Update hold order requests.
8. Check Current order status by orderId.
9. Update Sector resolution lists at runtime using previlaged operations (can be performed by previlaged users only.).
10.Get product sold by SellerId  
```
