package com.inventory.rest.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.inventory.initialization.setup.InventoryStatus;
import com.inventory.rest.impl.InventoryStatusHandlerImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@TestInstance(Lifecycle.PER_CLASS)
public class InventoryStatusHandlerTest {

	@Mock
	private InventoryStatus inventoryStatus;

	@InjectMocks
	@Spy
	private InventoryStatusHandlerImpl inventoryStatusHandlerImpl;
	
	@BeforeAll
	public void setupMocks() {
		MockitoAnnotations.initMocks(this);
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(InventoryStatusHandlerTest.class);

	/**
	 * Checks functionality of Inventory Status Handler.
	 * @throws JsonProcessingException
	 */
	@Test
	public void checkInventoryStatus() throws JsonProcessingException {

		when(inventoryStatus.getApplicationRunning()).thenReturn(true);
		when(inventoryStatus.getContainerId()).thenReturn("spring web application/Inventory");

		assertNotNull(inventoryStatus);
		assertNotNull(inventoryStatusHandlerImpl);
		
		/*
		  This is required because Jackson is throwing error stating FAIL_ON_EMPTY
		  as its unable to identify getters and setter of mock object.
		  So we create a writer so its aware of the methods at compile time.
		 */
		ObjectWriter objectWriter = new ObjectMapper().writerFor(InventoryStatus.class);

		LOGGER.info("Test method inventoryStatusHandlerImpl.getCurrentApplicationStatus() returned value : {}",
				objectWriter.writeValueAsString(inventoryStatusHandlerImpl.getCurrentApplicationStatus()));

		verify(inventoryStatusHandlerImpl, atLeastOnce()).getCurrentApplicationStatus();
		verify(inventoryStatus, atLeastOnce()).getApplicationRunning();
		verify(inventoryStatus, atLeastOnce()).getContainerId();
	}

}
