package com.inventory.rest.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inventory.constants.InventoryConstants;
import com.inventory.dto.UserDTO;
import com.inventory.initialization.setup.test.FullTestConfigurationSetup;
import com.inventory.rest.UserOperationsHandler;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringJUnitWebConfig
@ContextConfiguration(classes = FullTestConfigurationSetup.class)
public class UserOperationHandlerTest {

	@Autowired
	private UserOperationsHandler userOperationHandler;
	private static final Logger LOGGER = LoggerFactory.getLogger(UserOperationHandlerTest.class);
	/**
	 * This will ensure Unique Key is working and throw 2 exceptions.
	 * @throws JsonProcessingException 
	 */
	@Test
	@DisplayName("Check Save Method")
	public void checkSaveOrUpdate() throws JsonProcessingException {
		assertNotNull(userOperationHandler);
		UserDTO userDTO = new UserDTO();
		userDTO.setAccountActive(true);
		userDTO.setDateOfCreation(Instant.now());
		userDTO.setEmailId("joe@gmail.com");
		userDTO.setLastLogin(Instant.now());
		userDTO.setPrevilagedUser(true);
		userDTO.setUsername("joe");
		userDTO.setUserRole("admin");
		userDTO.setPassword("");
		
		UserDTO receivedValue = userOperationHandler.saveOrUpdateUser(userDTO); 
		LOGGER.info("Output Record is {} ",new ObjectMapper().writerFor(UserDTO.class).writeValueAsString(receivedValue));
		assertNotNull(receivedValue.getLastLogin());
		
		assertEquals(userOperationHandler.saveOrUpdateUser(userDTO).getUsername(),InventoryConstants.SAVEFAILED);
	}
}
