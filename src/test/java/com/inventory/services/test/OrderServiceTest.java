package com.inventory.services.test;

import com.inventory.constants.InventoryConstants;
import com.inventory.entities.*;
import com.inventory.initialization.setup.test.FullTestConfigurationSetup;
import com.inventory.services.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Chetan Garikapati
 * <br>
 * Created on : 3/11/18-Nov-2018
 */
@SpringJUnitWebConfig
@ContextConfiguration(classes = FullTestConfigurationSetup.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class OrderServiceTest {

    @Autowired
    private OrderService orderService;

    @Autowired
    private UserServices userServices;

    @Autowired
    private ProductService productService;

    @Autowired
    private WareHouseService wareHouseService;

    @Autowired
    private AddressService addressService;

    private OrderAccessBean savedOrderAccessBean;

    @BeforeEach
    public void saveOrderTest(){
        OrderAccessBean orderAccessBean = new OrderAccessBean();
        orderAccessBean.setDateOfCreation(Instant.now());
        orderAccessBean.setLastUpdated(Instant.now());
        orderAccessBean.setOrderStatus(InventoryConstants.ORDER_PENDING);
        orderAccessBean.setUserId("chetan@gmail.com");
        orderAccessBean.setInventoryAllocationStatus(InventoryConstants.HOLD_INVENTORY);
        orderAccessBean.setNumberOfItems(2);

        savedOrderAccessBean = orderService.saveOrUpdateOrder(orderAccessBean);
        assertNotNull(savedOrderAccessBean);
    }

    @Test
    public void deleteOrderTest(){
        assertTrue(orderService.deleteOrder(savedOrderAccessBean));
    }

    @Test
    public void testSaveOrderItem(){

        UserAccessBean userAccessBean1 = new UserAccessBean();
        userAccessBean1.setAccountActive(true);
        userAccessBean1.setEmailId("seller1@test.com");
        userAccessBean1.setPassword("password");
        userAccessBean1.setPrevilagedUser(true);
        userAccessBean1.setUsername("chetan");
        userAccessBean1.setUserRole("SELLER");

        Optional<UserAccessBean> optionalUserBean = userServices.saveorUpdateUser(userAccessBean1);
        assertNotNull(optionalUserBean.get());

        ProductAccessBean productAccessBean = new ProductAccessBean();
        productAccessBean.setAlertQty(10);
        productAccessBean.setDropShip(false);
        productAccessBean.setAvailableQty(56);
        productAccessBean.setManufacturer("GENERIC");
        productAccessBean.setPartNumber("AC105");
        productAccessBean.setSellerId(optionalUserBean.get());

        Optional<ProductAccessBean> optionalProductAccessBean = productService.saveOrUpdateProduct(productAccessBean);
        assertNotNull(optionalProductAccessBean.get());

        AddressAccessBean addressAccessBean = new AddressAccessBean();
        addressAccessBean.setCity("HYD");
        addressAccessBean.setState("TS");
        addressAccessBean.setRegion("SOUTH");
        addressAccessBean.setZipcode(500090);

        Optional<AddressAccessBean> optionalAddressAccessBean = addressService.saveOrUpdateAdddress(addressAccessBean);
        assertNotNull(optionalAddressAccessBean.get());

        WareHouseAccessBean wareHouseAccessBean = new WareHouseAccessBean();
        wareHouseAccessBean.setAddressDetails(optionalAddressAccessBean.get());
        wareHouseAccessBean.setWareHouseDefaultSectorId(1);
        wareHouseAccessBean.setWareHouseName("ProductInventory Test");

        Optional<WareHouseAccessBean> optionalWareHouseAccessBean = wareHouseService.saveOrUpdateWareHouse(wareHouseAccessBean);
        assertNotNull(optionalWareHouseAccessBean.get());

        OrderItemsAccessBean orderItemsAccessBean = new OrderItemsAccessBean();
        orderItemsAccessBean.setOrderId(savedOrderAccessBean);
        orderItemsAccessBean.setOrderItemStatus(InventoryConstants.ORDER_PENDING);
        orderItemsAccessBean.setProductId(optionalProductAccessBean.get());
        orderItemsAccessBean.setProductQuantity(1);
        orderItemsAccessBean.setInventoryAllocationStatus(InventoryConstants.HOLD_INVENTORY);
        orderItemsAccessBean.setWareHouseId(optionalWareHouseAccessBean.get());

        ArrayList<OrderItemsAccessBean> orderItemsAccessBeans = new ArrayList<>();
        orderItemsAccessBeans.add(orderItemsAccessBean);

        orderService.saveMultipleOrderItemsForExistingOrders(orderItemsAccessBeans);
    }
}
