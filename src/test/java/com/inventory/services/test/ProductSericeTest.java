package com.inventory.services.test;

import com.inventory.entities.ProductAccessBean;
import com.inventory.entities.UserAccessBean;
import com.inventory.initialization.setup.test.FullTestConfigurationSetup;
import com.inventory.services.ProductService;
import com.inventory.services.UserServices;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringJUnitWebConfig
@ContextConfiguration(classes = FullTestConfigurationSetup.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ProductSericeTest {

	@Autowired
	private ProductService productService;

	@Autowired
	private UserServices userServices;

	private Optional<UserAccessBean> optionalUserBean;

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductSericeTest.class);

	@Test
	public void testSaveOrUpdateProduct() {
		ProductAccessBean productAccessBean;

		UserAccessBean userAccessBean = new UserAccessBean();
		userAccessBean.setAccountActive(true);
		userAccessBean.setEmailId("seller@test45.com");
		userAccessBean.setPassword("password");
		userAccessBean.setPrevilagedUser(true);
		userAccessBean.setUsername("seller12");
		userAccessBean.setUserRole("SELLER");

		UserAccessBean userAccessBean1 = new UserAccessBean();
		userAccessBean1.setAccountActive(true);
		userAccessBean1.setEmailId("seller91@test.com");
		userAccessBean1.setPassword("password");
		userAccessBean1.setPrevilagedUser(true);
		userAccessBean1.setUsername("seller56");
		userAccessBean1.setUserRole("SELLER");

		optionalUserBean = userServices.saveorUpdateUser(userAccessBean);
		Optional<UserAccessBean> optionalUserBean1 = userServices.saveorUpdateUser(userAccessBean1);
		LOGGER.info("After saving user record {} ", optionalUserBean.get());
		assertTrue(optionalUserBean.isPresent());
		assertTrue(optionalUserBean1.isPresent());

		productAccessBean = new ProductAccessBean();
		productAccessBean.setAlertQty(10);
		productAccessBean.setDropShip(false);
		productAccessBean.setAvailableQty(56);
		productAccessBean.setManufacturer("GENERIC");
		productAccessBean.setPartNumber("AC1059");
		productAccessBean.setSellerId(optionalUserBean.get());

		Optional<ProductAccessBean> optionalProductAccessBean = productService.saveOrUpdateProduct(productAccessBean);
		assertNotNull(optionalProductAccessBean.get());
		LOGGER.info("After saving product record {} ", optionalProductAccessBean.get());
		assertNotNull(optionalProductAccessBean.get().getProductId());

		productAccessBean.setAlertQty(10);
		productAccessBean.setDropShip(false);
		productAccessBean.setAvailableQty(56);
		productAccessBean.setManufacturer("GENERIC");
		productAccessBean.setPartNumber("AC1095");
		productAccessBean.setSellerId(new UserAccessBean(102));

		optionalProductAccessBean = productService.saveOrUpdateProduct(productAccessBean);
		/**
		 * This will be null because FK Fails.
		 */
		assertFalse(optionalProductAccessBean.isPresent());

		assertFalse(productService.saveOrUpdateProduct(null).isPresent());

		ProductAccessBean productAccessBean1 = new ProductAccessBean();
		productAccessBean1.setAlertQty(10);
		productAccessBean1.setDropShip(false);
		productAccessBean1.setAvailableQty(56);
		productAccessBean1.setManufacturer("GENERIC");
		productAccessBean1.setPartNumber("AC10597");
		productAccessBean1.setSellerId(optionalUserBean1.get());

		Optional<ProductAccessBean> optionalProductAccessBean1 = productService.saveOrUpdateProduct(productAccessBean1);
		assertTrue(optionalProductAccessBean1.isPresent());
		assertNotNull(optionalProductAccessBean1.get());
		assertNotNull(optionalProductAccessBean1.get().getProductId());

		if (productService.findByProductId(optionalProductAccessBean1.get().getProductId()).isPresent()) {
			assertEquals(productService.deleteProduct(optionalProductAccessBean1.get().getProductId()), true);
			LOGGER.info("======================================================================");
			LOGGER.info("Product Removed Successfully");
			LOGGER.info("======================================================================");

		}

	}

	@Test
	public void testFindById() {
		Optional<ProductAccessBean> optionalProduct = productService.findByProductId(1L);
		assertEquals(optionalProduct.isPresent(), true);
		LOGGER.info("Loaded product by id : {} ", optionalProduct.get());
	}

	@Test
	public void testFindByPartNumber() {
		Optional<List<ProductAccessBean>> optionalProduct = productService.findByPartNumber("AC105");
		assertEquals(optionalProduct.isPresent(), true);
		LOGGER.info("Loaded product by partnumber : {} ", optionalProduct.get());
	}

	@Test
	public void testFindBySellerId() {
		Optional<List<ProductAccessBean>> optionalProduct = productService.findProductBySellerId(optionalUserBean.get().getUserId());
		assertEquals(optionalProduct.isPresent(), true);
		LOGGER.info("Loaded product by sellerId : {} ", optionalProduct.get());
	}

}
