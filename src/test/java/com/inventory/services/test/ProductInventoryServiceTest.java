package com.inventory.services.test;

import com.inventory.entities.*;
import com.inventory.initialization.setup.test.FullTestConfigurationSetup;
import com.inventory.services.*;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;

import java.time.Instant;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Chetan Garikapati
 * <br>
 * Created On : 27-Oct-2018
 */
@SpringJUnitWebConfig
@ContextConfiguration(classes = FullTestConfigurationSetup.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductInventoryServiceTest {


    private ProductInventoryService productInventoryService;
    private ProductService productService;
    private WareHouseService wareHouseService;
    private AddressService addressService;
    private UserServices userServices;

    private ProductInventoryAccessBean saveProductInventoryRecord;

    private Optional<ProductAccessBean> optionalProductAccessBean;
    private Optional<WareHouseAccessBean> optionalWareHouseAccessBean;

    @Autowired
    public ProductInventoryServiceTest(ProductInventoryService productInventoryService, ProductService productService, WareHouseService wareHouseService, AddressService addressService, UserServices userServices) {
        this.productInventoryService = productInventoryService;
        this.productService = productService;
        this.wareHouseService = wareHouseService;
        this.addressService = addressService;
        this.userServices = userServices;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductInventoryServiceTest.class);

    @BeforeAll
    public void setup() {
        AddressAccessBean addressAccessBean = new AddressAccessBean();
        addressAccessBean.setCity("HYD");
        addressAccessBean.setState("TS");
        addressAccessBean.setRegion("SOUTH");
        addressAccessBean.setZipcode(500090);

        Optional<AddressAccessBean> optionalAddressAccessBean = addressService.saveOrUpdateAdddress(addressAccessBean);
        assertNotNull(optionalAddressAccessBean.get());

        WareHouseAccessBean wareHouseAccessBean = new WareHouseAccessBean();
        wareHouseAccessBean.setAddressDetails(optionalAddressAccessBean.get());
        wareHouseAccessBean.setWareHouseDefaultSectorId(1);
        wareHouseAccessBean.setWareHouseName("ProductInventory Test12");

        this.optionalWareHouseAccessBean = wareHouseService.saveOrUpdateWareHouse(wareHouseAccessBean);
        assertNotNull(optionalWareHouseAccessBean.get());

        UserAccessBean userAccessBean = new UserAccessBean();
        userAccessBean.setAccountActive(true);
        userAccessBean.setPrevilagedUser(true);
        userAccessBean.setEmailId("johnny@gmail.com");
        userAccessBean.setPassword("abd");
        userAccessBean.setUserRole("admin");
        userAccessBean.setUsername("johnny");
        userAccessBean.setLastLogin(Instant.now());
        userAccessBean.setDateOfCreation(Instant.now());

        Optional<UserAccessBean> optionalUserAccessBean = userServices.saveorUpdateUser(userAccessBean);
        assertNotNull(optionalUserAccessBean.get());

        ProductAccessBean productAccessBean = new ProductAccessBean();
        assertNotNull(productAccessBean);
        productAccessBean.setAlertQty(10);
        productAccessBean.setAvailableQty(10);
        productAccessBean.setDropShip(false);
        productAccessBean.setManufacturer("GENERIC");
        productAccessBean.setPartNumber("AC1078");
        productAccessBean.setSellerId(optionalUserAccessBean.get());

        this.optionalProductAccessBean = productService.saveOrUpdateProduct(productAccessBean);
        assertNotNull(optionalProductAccessBean.get());
    }

    @Test
    public void test1_SaveProductInventory() {
        ProductInventoryAccessBean productInventoryAccessBean = new ProductInventoryAccessBean();
        productInventoryAccessBean.setAvailableQTY(10);
        productInventoryAccessBean.setWareHouseId(optionalWareHouseAccessBean.get());
        productInventoryAccessBean.setProductId(optionalProductAccessBean.get());

        saveProductInventoryRecord = productInventoryService.saveProductInventoryRecord(productInventoryAccessBean);
        assertNotNull(saveProductInventoryRecord.getProductInventoryId());

        LOGGER.info("Saved Product Inventory Record : {}", saveProductInventoryRecord);
    }

    @Test
    public void test2_FindByMethods() {

        LOGGER.info("Values of product and warehouse : {} ,{}", optionalProductAccessBean.get(), optionalWareHouseAccessBean.get());

        Optional<ProductInventoryAccessBean> inventoryByProductIdAndWareHouseId = productInventoryService.findInventoryByProductIdAndWareHouseId(optionalProductAccessBean.get(), optionalWareHouseAccessBean.get());
        assertNotNull(inventoryByProductIdAndWareHouseId.get());
        LOGGER.info("Returned Inventory List data : {} ", inventoryByProductIdAndWareHouseId.get());

        assertTrue(productInventoryService.deleteProductInventoryByProductInventoryId(saveProductInventoryRecord.getProductInventoryId()));
    }

}
