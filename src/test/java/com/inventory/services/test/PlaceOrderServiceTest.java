package com.inventory.services.test;

import com.inventory.constants.InventoryConstants;
import com.inventory.dto.OrderDTO;
import com.inventory.dto.ProductDTO;
import com.inventory.dto.ProductRequestBean;
import com.inventory.entities.AddressAccessBean;
import com.inventory.initialization.setup.test.FullTestConfigurationSetup;
import com.inventory.services.PlaceOrderService;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Chetan Garikapati
 * <br>
 * Created on : 10/11/18-Nov-2018
 */
@SpringJUnitWebConfig
@ContextConfiguration(classes = FullTestConfigurationSetup.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PlaceOrderServiceTest {

    @Autowired
    private PlaceOrderService placeOrderService;

    private OrderDTO orderDTO;

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductSericeTest.class);

    @Test
    public void test1_PlaceOrderHoldProducts(){
        ProductRequestBean productRequestBean = new ProductRequestBean();
        productRequestBean.setProductDetails(new ArrayList<ProductDTO>());
        ProductDTO productDTO = new ProductDTO();
        productDTO.setProductId(1L);
        productDTO.setProductQuantity(1);
        productRequestBean.getProductDetails().add(productDTO);

        productDTO = new ProductDTO();
        productDTO.setProductId(2L);
        productDTO.setProductQuantity(1);
        productRequestBean.getProductDetails().add(productDTO);

        AddressAccessBean addressAccessBean = new AddressAccessBean();
        addressAccessBean.setCity("HYD");
        productRequestBean.setDeliveryLocation(addressAccessBean);

        orderDTO = placeOrderService.placeOrder(productRequestBean);
        assertNotNull(orderDTO);
        assertNotEquals(InventoryConstants.RESOLUTION_FAILED,orderDTO.getOrderStatus());
        LOGGER.info("OrderDTO Record : {}",orderDTO);
    }

    @Test
    public void test2_ConfirmOrder(){

        orderDTO = placeOrderService.confirmOrder(orderDTO);
        assertNotNull(orderDTO);
    }

    @Test
    public void testCancelOrder(){
        orderDTO = placeOrderService.cancelOrder(orderDTO);
        assertEquals(InventoryConstants.ORDER_CANCELLED,orderDTO.getOrderStatus());
    }
}
