package com.inventory.services.test;

import com.inventory.entities.AddressAccessBean;
import com.inventory.initialization.setup.test.FullTestConfigurationSetup;
import com.inventory.services.AddressService;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;

import java.util.Optional;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringJUnitWebConfig
@ContextConfiguration(classes=FullTestConfigurationSetup.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AddressServiceTest {

	@Autowired
	private AddressService addressService;

	private AddressAccessBean addressAccessBean;

	private static final Logger LOGGER = LoggerFactory.getLogger(AddressServiceTest.class);
	
	@Test
	public void test1_SaveOrUpdateRecord() {
		this.addressAccessBean = new AddressAccessBean();
		addressAccessBean.setCity("HYD");
		addressAccessBean.setRegion("SOUTH");
		addressAccessBean.setState("TS");
		addressAccessBean.setZipcode(500090);
		
		
		Optional<AddressAccessBean> optionalAddressAccessBean = addressService.saveOrUpdateAdddress(addressAccessBean);
		assertTrue(optionalAddressAccessBean.isPresent());
		addressAccessBean = optionalAddressAccessBean.get();
		LOGGER.info("After save AddressAccessBean is : {} ",optionalAddressAccessBean.get());		
		assertThat(optionalAddressAccessBean.get().getAddressId(), greaterThan(0));
		
		//Trying to save again should not fire an insert statement.
		assertNotNull(addressService.saveOrUpdateAdddress(addressAccessBean).get());
		assertFalse(addressService.saveOrUpdateAdddress(null).isPresent());	
				
	}
	
	@Test
	public void test2GetById() {
		Optional<AddressAccessBean> optionalAddress;		
		optionalAddress = addressService.getAddressRecordById(addressAccessBean.getAddressId());
		
		assertEquals(optionalAddress.isPresent(), true);
		LOGGER.info("Retreived Reord is {} ", optionalAddress.get());
		
		optionalAddress = addressService.getAddressRecordById(-1);
		assertEquals(optionalAddress.isPresent(), false);
	}
	
	@Test	
	public void test3_DeleteRecord() {
		assertEquals(addressService.deleteAddressRecord(addressAccessBean.getAddressId()), true);
		assertEquals(addressService.deleteAddressRecord(addressAccessBean.getAddressId()), false);
		assertEquals(addressService.deleteAddressRecord(addressAccessBean.getAddressId()+12), false);
	}
}
