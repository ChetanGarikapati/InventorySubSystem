package com.inventory.components.test;

import com.inventory.entities.AddressAccessBean;
import com.inventory.entities.SectorAccessBean;
import com.inventory.initialization.setup.PrepareInventoryAllocationSetup;
import com.inventory.initialization.setup.test.FullTestConfigurationSetup;
import com.inventory.services.SectorService;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.ConcurrentMap;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringJUnitWebConfig
@ContextConfiguration(classes=FullTestConfigurationSetup.class)
public class PrepareInventoryAllocationSetupTest {

	@Autowired
	private PrepareInventoryAllocationSetup prepareInventoryAllocationSetupTest;
	
	@Autowired
	private SectorService sectorService;
	
	@Autowired
	WebApplicationContext webApplicationContext;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PrepareInventoryAllocationSetupTest.class);
	
	@SuppressWarnings({ "unchecked"})
	@RepeatedTest(value=3)
	public void checkResolutionMapCreation(RepetitionInfo repetitionInfo) {
		AddressAccessBean addressAccessBean = new AddressAccessBean();
		addressAccessBean.setCity("HYD");
		addressAccessBean.setState("TS");
		addressAccessBean.setRegion("SOUTH");
		addressAccessBean.setZipcode(500090);
						
		SectorAccessBean sectorAccessBean = new SectorAccessBean();
		sectorAccessBean.setPriority(1);		
		sectorAccessBean.setSectorIdentificationArea(addressAccessBean);
		
		//1ST INSERT
		Optional<SectorAccessBean> optionalSector = sectorService.saveOrUpdateSector(sectorAccessBean);
		assertTrue(optionalSector.isPresent());
		addressAccessBean = optionalSector.get().getSectorIdentificationArea();
		
		sectorAccessBean.setSectorIdentificationArea(addressAccessBean);
		
		//2ND INSERT
		sectorAccessBean.setPriority(2);
		optionalSector = sectorService.saveOrUpdateSector(sectorAccessBean);
		assertTrue(optionalSector.isPresent());
		
		//3RD INSERT
		sectorAccessBean.setPriority(3);
		optionalSector = sectorService.saveOrUpdateSector(sectorAccessBean);
		assertTrue(optionalSector.isPresent());
				
		ConcurrentMap<String, ArrayList<Integer>> citySectorMap = webApplicationContext.getBean("CitySectorMap", ConcurrentMap.class);
		ConcurrentMap<String, ArrayList<Integer>> stateSectorMap = webApplicationContext.getBean("StateSectorMap", ConcurrentMap.class);
		ConcurrentMap<String, ArrayList<Integer>> regionSectorMap = webApplicationContext.getBean("RegionSectorMap", ConcurrentMap.class);
		assertNotNull(citySectorMap);


		//THIS IS COMMENTED BECAUSE WE ARE LOADING DEFAULT DATA ON STARTUP , UNCOMMENT WHEN NO DEFAULT DATA IS LOADED.
		/*
		if(repetitionInfo.getCurrentRepetition() < 2) {
			assertTrue(citySectorMap.isEmpty());
		}
		*/
		
		prepareInventoryAllocationSetupTest.prepareRegistryForInventoryAllocation();
		assertFalse(citySectorMap.isEmpty());
		LOGGER.info("CITY MAP IS {} ",citySectorMap);
		LOGGER.info("STATE MAP IS {} ",stateSectorMap);
		LOGGER.info("REGION MAP IS {} ",regionSectorMap);
	}
}
