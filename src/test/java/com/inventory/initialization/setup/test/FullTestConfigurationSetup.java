package com.inventory.initialization.setup.test;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletContext;
import java.io.InputStream;
import java.util.Properties;


/**
 * <p>
 * This is the base class for setting up the configurations of the application. </br>
 * This class setups JPA and Transaction Management. </br>
 * Provide custom configurations in this class. </br>
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 20-Oct-2018
 */
@Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan(basePackages = "com.inventory")
public class FullTestConfigurationSetup {

    private final String jpaPropertiesPath;
    private final String inventoryConfigurationPropertiesPath;
    private static final Logger LOGGER = LoggerFactory.getLogger(FullTestConfigurationSetup.class);

    @Autowired
    public FullTestConfigurationSetup(ServletContext servletContext) {
        setLoggingLevels();
        this.jpaPropertiesPath = "META-INF/Test/TestJPAProperties.properties";
        this.inventoryConfigurationPropertiesPath = "META-INF/Test/InventoryConfigurations.properties";
        LOGGER.info("Loading JPA-Properties from file {} ", this.jpaPropertiesPath);
        LOGGER.info("Loading InventoryConfiguration-Properties from file {} ", this.inventoryConfigurationPropertiesPath);
    }

    /**
     * Setups JPA Properties by loading from META-INF/JPAProperties.properties file unless specified.</br>
     * The location should be specified in <strong>InventorySystemInitializer Class </strong>.
     *
     * @return Properties
     */
    @Bean(name = "JPA_Properties")
    public Properties jpaProperties() {
        Properties properties = new Properties();

        try (InputStream fileInputStream = getClass().getClassLoader().getResourceAsStream(jpaPropertiesPath)) {
            properties.load(fileInputStream);
        } catch (Exception e) {
            LOGGER.error("Unable load JPA Properties,This will result in application crash ", e);
        }
        return properties;
    }

    /**
     * Setups JPAVendor Adapter by default uses HibernateVendorAdapter.
     *
     * @return JpaVendorAdapter
     */
    @Bean(name = "HibernateJPAVendorAdapter")
    public JpaVendorAdapter setupJPAVendorAdapter() {
        return new HibernateJpaVendorAdapter();
    }

    /**
     * Setups EntityManagerFactory by default creates <strong>LocalContainerEntityManagerFactoryBean</strong>.
     *
     * @param jpaProperties    Properties files containing JPA Properties.
     * @param jpaVendorAdapter JPAVendorAdapter instance.
     * @return LocalContainerEntityManagerFactoryBean
     */
    @Bean(name = "EntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean setupJPA(
            @Autowired @Qualifier(value = "JPA_Properties") Properties jpaProperties,
            @Autowired JpaVendorAdapter jpaVendorAdapter) {

        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setJpaProperties(jpaProperties);
        entityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter);
        entityManagerFactoryBean.setPackagesToScan("com.inventory");
        entityManagerFactoryBean.setPersistenceUnitName("Inventory_PersistenceUnit");
        return entityManagerFactoryBean;
    }

    /**
     * Setups TransactionManagement, by default creates  <strong>JpaTransactionManager</strong> change to
     * JTATransactionManager when deploying in ApplicationServer to delegate transaction management.
     *
     * @param entityManagerFactory
     * @return PlatformTransactionManager
     */
    @Bean(name = "JPATransactionManager")
    public PlatformTransactionManager setupJPATransactionManagement(
            @Autowired EntityManagerFactory entityManagerFactory) {

        return new JpaTransactionManager(entityManagerFactory);
    }

    /**
     * This setups ModelMapper for Converting DTO TO Entities and other uses.
     *
     * @return ModelMapper
     */
    @Bean(name = "SimpleModelMapper")
    public ModelMapper setupModelMapperForObjectTranslation() {
        return new ModelMapper();
    }

    private void setLoggingLevels() {
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        ch.qos.logback.classic.Logger logger = loggerContext.getLogger("org");
        logger.setLevel(Level.INFO);
    }

    @Bean(value = "InventoryConfiguration_Properties")
    public Properties inventoryConfigurationProperties() {
        Properties properties = new Properties();

        try (InputStream fileInputStream = getClass().getClassLoader().getResourceAsStream(inventoryConfigurationPropertiesPath)) {
            properties.load(fileInputStream);
        } catch (Exception e) {
            LOGGER.error("Unable to load InventoryConfiguration Properties from {} , will result in broken functionality ", inventoryConfigurationPropertiesPath);
        }
        return properties;
    }

    @Bean(value = "GLOBAL_SECTOR_FALLBACK")
    public Boolean setupGlobalSectorFallback(@Autowired @Qualifier(value = "InventoryConfiguration_Properties") Properties properties) {
        Boolean configuredSectorFallback;
        if (properties.containsKey("EnableGlobalSectorFallback")) {
            configuredSectorFallback = Boolean.valueOf(properties.getProperty("EnableGlobalSectorFallback"));
            LOGGER.info("EnableGlobalSectorFallback is configured to : {} ", configuredSectorFallback);
            return configuredSectorFallback;
        }
        configuredSectorFallback = false;
        LOGGER.warn("EnableGlobalSectorFallback is not configured returning FALSE");
        return configuredSectorFallback;
    }


}
