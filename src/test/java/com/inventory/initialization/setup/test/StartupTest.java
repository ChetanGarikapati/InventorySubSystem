package com.inventory.initialization.setup.test;

import com.inventory.dao.SectorDAO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringJUnitWebConfig
@ContextConfiguration(classes = FullTestConfigurationSetup.class)
public class StartupTest {

	@Autowired
	private WebApplicationContext webApplicationContext;
	private static final Logger LOGGER = LoggerFactory.getLogger(StartupTest.class);

	@Test
	@DisplayName("Spring Context Setup Test And Bean Check")
	public void contextSetupTest() {
		assertNotNull(webApplicationContext);
		LOGGER.info("Configuration setup successful, application status : {} ", webApplicationContext.getStartupDate());
		assertNotNull(webApplicationContext.getBean(SectorDAO.class));
		LOGGER.info("SectorDAO Implementation Class is : {} ",
				webApplicationContext.getBean(SectorDAO.class).getClass().getName());
	}
}
