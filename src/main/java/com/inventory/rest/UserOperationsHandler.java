package com.inventory.rest;

import com.inventory.dto.UserDTO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface UserOperationsHandler {

    @PostMapping("/login")
    String userLogin(@RequestBody UserDTO userDTO);

    @PostMapping("/update-users")
    UserDTO saveOrUpdateUser(@RequestBody UserDTO userDTO);

    @PostMapping("/disable-account")
    boolean disableUserAccount(@RequestBody String emailId);

}
