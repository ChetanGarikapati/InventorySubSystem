package com.inventory.rest.impl;

import com.inventory.constants.InventoryConstants;
import com.inventory.dto.UserDTO;
import com.inventory.entities.UserAccessBean;
import com.inventory.rest.UserOperationsHandler;
import com.inventory.services.UserServices;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/user-operations")
public class UserOperationsHandlerImpl implements UserOperationsHandler {

    private UserServices userServices;
    private ModelMapper modelMapper;

    @Autowired
    public UserOperationsHandlerImpl(UserServices userServices, ModelMapper modelMapper) {
        this.userServices = userServices;
        this.modelMapper = modelMapper;
    }

    @Override
    @PostMapping("/login")
    public String userLogin(@RequestBody UserDTO userDTO) {
        if (userServices.login(userDTO)) {
            return InventoryConstants.LOGINSUCCESSFUL;
        }
        return InventoryConstants.INVALIDCREDENTIALS;
    }

    @Override
    @PostMapping("/update-users")
    public UserDTO saveOrUpdateUser(@RequestBody UserDTO userDTO) {
        Optional<UserAccessBean> userAccessBean = userServices
                .saveorUpdateUser(modelMapper.map(userDTO, UserAccessBean.class));
        if (userAccessBean.isPresent()) {
            return modelMapper.map(userAccessBean.get(), UserDTO.class);
        }

        userDTO.setUsername(InventoryConstants.SAVEFAILED);
        userDTO.setEmailId(InventoryConstants.SAVEFAILED);
        userDTO.setUserId(-1);
        return userDTO;
    }

    @Override
    @PostMapping("/disable-account")
    public boolean disableUserAccount(@RequestBody String emailId) {
        return userServices.disableUserAccount(null, emailId);
    }

}
