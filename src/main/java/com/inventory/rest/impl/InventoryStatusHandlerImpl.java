package com.inventory.rest.impl;

import com.inventory.initialization.setup.InventoryStatus;
import com.inventory.rest.InventoryStatusHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Base class for Inventory Status REST Implementation.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 21-Oct-2018
 */
@RestController
@RequestMapping("/status")
public class InventoryStatusHandlerImpl implements InventoryStatusHandler {

    private InventoryStatus inventoryStatus;

    @Autowired
    public InventoryStatusHandlerImpl(InventoryStatus inventoryStatus) {
        this.inventoryStatus = inventoryStatus;
    }

    /**
     * Provides current status of the application.
     *
     * @return InventoryStatus
     */
    @Override
    @GetMapping("/current-status")
    public InventoryStatus getCurrentApplicationStatus() {
        return inventoryStatus;
    }

}
