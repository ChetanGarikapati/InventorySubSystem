package com.inventory.rest;

import com.inventory.initialization.setup.InventoryStatus;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * The base template class for providing REST Implementation class for InventorySystem Status.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 21-Oct-2018
 */
public interface InventoryStatusHandler {

    /**
     * Provides current status of the application.
     *
     * @return InventoryStatus
     */
    @GetMapping("/current-status")
    InventoryStatus getCurrentApplicationStatus();

}
