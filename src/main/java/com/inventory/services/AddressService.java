package com.inventory.services;

import com.inventory.entities.AddressAccessBean;

import java.util.Optional;

/**
 * The base template for Service Implementation class for providing Address Operations.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 20-Oct-2018
 */
public interface AddressService {

    /**
     * Save new record or updates existing records.
     *
     * @param addressAccessBean The record to be saved.
     * @return Optional&lt;AddressAccessBean&gt;
     */
    Optional<AddressAccessBean> saveOrUpdateAdddress(AddressAccessBean addressAccessBean);

    /**
     * Find Address Record by addressId.
     *
     * @param addressId The id for expected record.
     * @return Optional&lt;AddressAccessBean&gt;
     */
    Optional<AddressAccessBean> getAddressRecordById(Integer addressId);

    /**
     * Deletes an existing record and return if delete was successful.
     *
     * @param addressAccessBean The record to be deleted.
     * @return boolean
     */
    boolean deleteAddressRecord(AddressAccessBean addressAccessBean);

    /**
     * Deletes an existing record and return if delete was successful.
     *
     * @param addressId The addressId of the record.
     * @return booleans
     */
    boolean deleteAddressRecord(Integer addressId);
}
