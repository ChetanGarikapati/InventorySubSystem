package com.inventory.services;

import com.inventory.entities.SectorAccessBean;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * The base template for Service Implementation class for providing Sector Operations.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 20-Oct-2018
 */
public interface SectorService {

    /**
     * Saves new record or updates existing record.
     *
     * @param sectorAccessBean The record to be saved.
     * @return Optional&lt;SectorAccessBean&gt;
     */
    Optional<SectorAccessBean> saveOrUpdateSector(SectorAccessBean sectorAccessBean);

    /**
     * Deletes an existing sector record.
     *
     * @param sectorAccessBean The record to be deleted.
     * @return boolean
     */
    boolean deleteSector(SectorAccessBean sectorAccessBean);

    /**
     * Deletes an existing sector record.
     *
     * @param sectorId The sectorId of the record to be deleted.
     * @return boolean
     */
    boolean deleteSector(Integer sectorId);

    /**
     * Update the existing priority of sector, and return if update was successful.
     *
     * @param sectorAccessBean
     * @return boolean
     */
    @Transactional
    boolean updateSectorPriority(SectorAccessBean sectorAccessBean);

    /**
     * Find list of sectors with specified priority.
     *
     * @param priority The priority for which sectors are required.
     * @return Optional&lt;List&lt;SectorAccessBean&gt;&gt;
     */
    Optional<List<SectorAccessBean>> findSectorsByPriority(Integer priority);

    /**
     * Find all sectors sorted by priorities.
     *
     * @param getSectorPriorityByASC if true the values are returned sorted by
     *                               priority in Ascending order else returns in
     *                               Descending order.
     * @return Optional&lt;List&lt;SectorAccessBean&gt;&gt;
     */
    Optional<List<SectorAccessBean>> findAllSectorsSortedByPriority(Boolean getSectorPriorityByASC);

}
