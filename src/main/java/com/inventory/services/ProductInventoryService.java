package com.inventory.services;

import com.inventory.entities.ProductAccessBean;
import com.inventory.entities.ProductInventoryAccessBean;
import com.inventory.entities.WareHouseAccessBean;

import java.util.List;
import java.util.Optional;

/**
 * The base template for Service Implementation class for providing ProductInventory Operations.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 27-Oct-2018
 */
public interface ProductInventoryService {

    public ProductInventoryAccessBean saveProductInventoryRecord(ProductInventoryAccessBean productInventoryAccessBean);

    public Optional<ProductInventoryAccessBean> findProductInventoryByProductInventoryId(Long productInventoryId);

    public Optional<ProductInventoryAccessBean> findInventoryByProductIdAndWareHouseId(ProductAccessBean productId, WareHouseAccessBean wareHouseId);

    public Boolean deleteProductInventoryById(ProductInventoryAccessBean productInventoryAccessBean);

    public Boolean deleteProductInventoryByProductInventoryId(Long productInventoryId);

    public Optional<List<ProductInventoryAccessBean>> findInventoryByProductIdByWareHouseIds(ProductAccessBean productId, List<WareHouseAccessBean> wareHouseIds);
}
