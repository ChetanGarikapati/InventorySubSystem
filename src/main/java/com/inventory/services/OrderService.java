package com.inventory.services;

import com.inventory.entities.OrderAccessBean;
import com.inventory.entities.OrderItemsAccessBean;

import java.util.List;
import java.util.Optional;

/**
 * This is the base template for class implementing OrderServices.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 3/11/18-Nov-2018
 */
public interface OrderService {

    public Optional<OrderAccessBean> findOrderByOrderId(Long orderId);

    public OrderAccessBean saveOrUpdateOrder(OrderAccessBean orderAccessBean);

    public OrderItemsAccessBean saveOrUpdateOrderItemForExistingProduct(OrderItemsAccessBean orderItemsAccessBean);

    public Boolean deleteOrder(OrderAccessBean orderAccessBean);

    public Optional<OrderItemsAccessBean>  findOrderItemByOrderItemId(Long orderItemId);

    public List<OrderItemsAccessBean> saveMultipleOrderItemsForExistingOrders(List<OrderItemsAccessBean> orderItemsAccessBeans);

    public Boolean deleteOrderItem(OrderItemsAccessBean orderItemsAccessBean);
}
