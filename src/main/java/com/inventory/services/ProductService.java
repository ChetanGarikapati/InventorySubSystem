package com.inventory.services;

import com.inventory.entities.ProductAccessBean;

import java.util.List;
import java.util.Optional;

/**
 * The base template for Service Implementation class for providing product Operations.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 20-Oct-2018
 */
public interface ProductService {

    /**
     * Saves new record or updates record and return if operation is successful.
     *
     * @param productAccessBean The record to be saved.
     * @return Optional&lt;ProductAccessBean&gt;
     */
    Optional<ProductAccessBean> saveOrUpdateProduct(ProductAccessBean productAccessBean);

    /**
     * Finds Product details by productId.
     *
     * @param productId The productId of the Product.
     * @return Optional&lt;ProductAccessBean&gt;
     */
    Optional<ProductAccessBean> findByProductId(Long productId);

    /**
     * Finds Product details by product partnumber.
     *
     * @param partNumber Partnumber of the product.
     * @return Optional&lt;List&lt;ProductAccessBean&gt;&gt;
     */
    Optional<List<ProductAccessBean>> findByPartNumber(String partNumber);

    /**
     * Find all products by sellerId.
     *
     * @param sellerId The sellerId of the product seller.
     * @return Optional&lt;List&lt;ProductAccessBean&gt;&gt;
     */
    Optional<List<ProductAccessBean>> findProductBySellerId(Integer sellerId);

    /**
     * Deletes the product record.
     *
     * @param productAccessBean The product record to be deleted.
     * @return boolean
     */
    boolean deleteProduct(ProductAccessBean productAccessBean);

    /**
     * Deletes the product record.
     *
     * @param productId The productId of record to be deleted.
     * @return boolean
     */
    boolean deleteProduct(Long productId);
}
