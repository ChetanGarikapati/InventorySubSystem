package com.inventory.services.impl;

import com.inventory.constants.InventoryConstants;
import com.inventory.dao.SectorDAO;
import com.inventory.entities.SectorAccessBean;
import com.inventory.services.SectorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * The base implementation class SectorService.
 *
 * @author Chetan Garikapati <br>
 * Created on : 20-Oct-2018
 */
@Service(value = "BaseSectorServiceImplementation")
public class SectorServiceImplementation implements SectorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SectorServiceImplementation.class);
    private SectorDAO sectorDAO;

    @Autowired
    public SectorServiceImplementation(SectorDAO sectorDAO) {
        this.sectorDAO = sectorDAO;
    }

    /**
     * Saves new record or updates existing record.
     *
     * @param sectorAccessBean The record to be saved.
     * @return Optional&lt;SectorAccessBean&gt;
     */
    @Override
    public Optional<SectorAccessBean> saveOrUpdateSector(SectorAccessBean sectorAccessBean) {
        try {
            return Optional.ofNullable(sectorDAO.saveOrUpdateSector(sectorAccessBean));
        } catch (Exception e) {
            LOGGER.error("Saving Sector Record {} failed ", e);
            return Optional.empty();
        }
    }

    /**
     * Deletes an existing sector record.
     *
     * @param sectorAccessBean The record to be deleted.
     * @return boolean
     */
    @Override
    public boolean deleteSector(SectorAccessBean sectorAccessBean) {
        try {
            return sectorDAO.deleteSector(sectorAccessBean);
        } catch (Exception e) {
            LOGGER.error("Deleting Sector Record {} failed ", e);
            return InventoryConstants.OPERATIONFAILED;
        }
    }

    /**
     * Deletes an existing sector record.
     *
     * @param sectorId The sectorId of the record to be deleted.
     * @return boolean
     */
    @Override
    public boolean deleteSector(Integer sectorId) {
        return deleteSector(new SectorAccessBean(sectorId));
    }

    /**
     * Update the existing priority of sector, and return if update was successful.
     *
     * @param sectorAccessBean
     * @return boolean
     */
    @Override
    @Transactional
    public boolean updateSectorPriority(SectorAccessBean sectorAccessBean) {
        Optional<SectorAccessBean> currentSectorPriority = sectorDAO.findBySectorId(sectorAccessBean.getSectorId());
        if (currentSectorPriority.isPresent()) {
            LOGGER.info("Updating Sector Priority from {} to {} ", sectorAccessBean, currentSectorPriority.get());
            currentSectorPriority.get().setPriority(sectorAccessBean.getPriority());
            return InventoryConstants.OPERATIONSUCCESSFUL;
        }
        return InventoryConstants.OPERATIONFAILED;
    }

    /**
     * Find list of sectors with specified priority.
     *
     * @param priority The priority for which sectors are required.
     * @return Optional&lt;List&lt;SectorAccessBean&gt;&gt;
     */
    @Override
    public Optional<List<SectorAccessBean>> findSectorsByPriority(Integer priority) {
        return sectorDAO.findSectorsByPriority(priority);
    }

    /**
     * Find all sectors sorted by priorities.
     *
     * @param getSectorPriorityByASC if true the values are returned sorted by
     *                               priority in Ascending order else returns in
     *                               Descending order.
     * @return Optional&lt;List&lt;SectorAccessBean&gt;&gt;
     */
    @Override
    public Optional<List<SectorAccessBean>> findAllSectorsSortedByPriority(Boolean getSectorPriorityByASC) {
        return sectorDAO.findSectorsBySortedPriority(getSectorPriorityByASC);
    }

}
