package com.inventory.services.impl;

import com.inventory.constants.InventoryConstants;
import com.inventory.dao.SectorWareHousePriorityDAO;
import com.inventory.entities.SectorWareHousePriorityAccessBean;
import com.inventory.services.SectorWareHousePriorityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * The base implementation class SectorWareHousePriorityService.
 *
 * @author Chetan Garikapati <br>
 * Created on : 20-Oct-2018
 */
@Service(value = "BaseSectorWareHousePriorityServiceImplementation")
public class SectorWareHousePriorityServiceImplementation implements SectorWareHousePriorityService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SectorWareHousePriorityServiceImplementation.class);
    private SectorWareHousePriorityDAO sectorWareHousePriorityDAO;

    @Autowired
    public SectorWareHousePriorityServiceImplementation(SectorWareHousePriorityDAO sectorWareHousePriorityDAO) {
        this.sectorWareHousePriorityDAO = sectorWareHousePriorityDAO;
    }

    /**
     * Find warehouse priority for a given sector by sectorWareHousePriorityId.
     *
     * @param sectorWareHousePriorityId The sectorWareHousePriorityId for the
     *                                  record.
     * @return Optional&lt;SectorWareHousePriorityAccessBean&gt;
     */
    @Override
    public Optional<SectorWareHousePriorityAccessBean> findBySectorWareHousePriorityId(
            Integer sectorWareHousePriorityId) {
        return sectorWareHousePriorityDAO.findBySectorWareHousePriorityId(sectorWareHousePriorityId);
    }

    /**
     * Find priority of all warehouses in a given sector.
     *
     * @param sectorId                         The sectorId for which priorities are
     *                                         required.
     * @param getWareHousesSortedByPriorityASC Specify if warehouses returned be
     *                                         sorted by priority in Ascending order
     * @return Optional&lt;List&lt;SectorWareHousePriorityAccessBean&gt;&gt;
     */
    @Override
    public Optional<List<SectorWareHousePriorityAccessBean>> findWareHousePriorityBySectorId(Integer sectorId,
                                                                                             Boolean getWareHousesSortedByPriorityASC) {
        return sectorWareHousePriorityDAO.findAllWareHousesBySectorId(sectorId, getWareHousesSortedByPriorityASC);
    }

    /**
     * Updates warehouse priority for a given sector and returns if update is
     * successful.
     *
     * @param sectorWareHousePriorityAccessBean
     * @return boolean
     */
    @Override
    @Transactional
    public boolean updateSectorWareHousePriority(SectorWareHousePriorityAccessBean sectorWareHousePriorityAccessBean) {

        Optional<SectorWareHousePriorityAccessBean> currentRecord = sectorWareHousePriorityDAO
                .findBySectorAndWareHouseId(sectorWareHousePriorityAccessBean.getSectorId().getSectorId(),
                        sectorWareHousePriorityAccessBean.getWareHouseId().getWareHouseId());
        if (currentRecord.isPresent()) {
            LOGGER.info("Updating Priority from {} to {} ", currentRecord.get(), sectorWareHousePriorityAccessBean);
            currentRecord.get().setPriority(sectorWareHousePriorityAccessBean.getPriority());
            return InventoryConstants.OPERATIONSUCCESSFUL;
        }
        return InventoryConstants.OPERATIONFAILED;
    }

    /**
     * Saves new record or updates existing record.
     *
     * @param sectorWareHousePriorityAccessBean The record to be saved.
     * @return Optional&lt;SectorWareHousePriorityAccessBean&gt;
     */
    @Override
    public Optional<SectorWareHousePriorityAccessBean> saveOrUpdateSectorWareHousePriority(
            SectorWareHousePriorityAccessBean sectorWareHousePriorityAccessBean) {
        try {
            return Optional.ofNullable(
                    sectorWareHousePriorityDAO.saveOrUpdateWareHousePriority(sectorWareHousePriorityAccessBean));
        } catch (Exception e) {
            LOGGER.error("Failed Saving Record {} ", sectorWareHousePriorityAccessBean, e);
            return Optional.empty();
        }
    }

    /**
     * Deletes an existing record.
     *
     * @param sectorWareHousePriorityAccessBean The record to be deleted.
     * @return boolean
     */
    @Override
    public boolean deleteSectorWareHousePriorityRecord(
            SectorWareHousePriorityAccessBean sectorWareHousePriorityAccessBean) {
        try {
            return sectorWareHousePriorityDAO.deleteSectorWareHousePriority(sectorWareHousePriorityAccessBean);
        } catch (Exception e) {
            LOGGER.error("Failed Deleting Record {} ", sectorWareHousePriorityAccessBean, e);
            return InventoryConstants.OPERATIONFAILED;
        }
    }

    /**
     * Deletes an existing record.
     *
     * @param sectorWareHousePriorityId The record id to be deleted.
     * @return boolean
     */
    @Override
    public boolean deleteSectorWareHousePriorityRecord(Integer sectorWareHousePriorityId) {
        return deleteSectorWareHousePriorityRecord(new SectorWareHousePriorityAccessBean(sectorWareHousePriorityId));
    }
}
