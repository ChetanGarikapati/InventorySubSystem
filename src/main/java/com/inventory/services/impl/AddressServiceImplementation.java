package com.inventory.services.impl;

import com.inventory.constants.InventoryConstants;
import com.inventory.dao.AddressDAO;
import com.inventory.entities.AddressAccessBean;
import com.inventory.services.AddressService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * The base implementation class AddressService.
 *
 * @author Chetan Garikapati <br>
 * Created on : 20-Oct-2018
 */
@Service(value = "BaseAddressServiceImplementation")
public class AddressServiceImplementation implements AddressService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AddressServiceImplementation.class);
    private AddressDAO addressDAO;

    @Autowired
    public AddressServiceImplementation(AddressDAO addressDAO) {
        this.addressDAO = addressDAO;
    }

    /**
     * Save new record or updates existing records.
     *
     * @param addressAccessBean The record to be saved.
     * @return Optional&lt;AddressAccessBean&gt;
     */
    @Override
    public Optional<AddressAccessBean> saveOrUpdateAdddress(AddressAccessBean addressAccessBean) {
        try {
            return Optional.ofNullable(addressDAO.saveOrUpdateAddress(addressAccessBean));
        } catch (Exception e) {
            LOGGER.error("Saving Address Record {} failed ", addressAccessBean, e);
            return Optional.empty();
        }
    }

    /**
     * Find Address Record by addressId.
     *
     * @param addressId The id for expected record.
     * @return Optional&lt;AddressAccessBean&gt;
     */
    @Override
    public Optional<AddressAccessBean> getAddressRecordById(Integer addressId) {
        return addressDAO.findByAddressId(addressId);
    }

    /**
     * Deletes an existing record and return if delete was successful.
     *
     * @param addressAccessBean The record to be deleted.
     * @return boolean
     */
    @Override
    public boolean deleteAddressRecord(AddressAccessBean addressAccessBean) {
        try {
            return addressDAO.deleteAddress(addressAccessBean);
        } catch (Exception e) {
            LOGGER.error("Deleting Address Record {} failed ", addressAccessBean, e);
            return InventoryConstants.OPERATIONFAILED;
        }
    }

    /**
     * Deletes an existing record and return if delete was successful.
     *
     * @param addressId The addressId of the record.
     * @return booleans
     */
    @Override
    public boolean deleteAddressRecord(Integer addressId) {
        return deleteAddressRecord(new AddressAccessBean(addressId));
    }
}
