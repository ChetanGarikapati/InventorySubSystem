package com.inventory.services.impl;

import com.inventory.constants.InventoryConstants;
import com.inventory.dao.UsersDAO;
import com.inventory.dto.UserDTO;
import com.inventory.entities.UserAccessBean;
import com.inventory.services.UserServices;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * The base implementation class of UserServices.
 *
 * @author Chetan Garikapati <br>
 * Created on : 20-Oct-2018
 */
@Service(value = "BaseUserServiceImplementation")
public class UserServiceImplementation implements UserServices {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImplementation.class);
    private UsersDAO usersDAO;
    private ModelMapper modelMapper;

    @Autowired
    public UserServiceImplementation(UsersDAO usersDAO, ModelMapper modelMapper) {
        this.usersDAO = usersDAO;
        this.modelMapper = modelMapper;
    }

    /**
     * Saves new user record or Updates existing records.
     *
     * @param userAccessBean The record to be saved.
     * @return Optional&lt;UserAccessBean&gt;
     */
    @Override
    public Optional<UserAccessBean> saveorUpdateUser(UserAccessBean userAccessBean) {
        try {
            return Optional.ofNullable(usersDAO.saveOrUpdateUser(userAccessBean));
        } catch (Exception e) {
            LOGGER.error("Error Saving Record {} ", userAccessBean, e);
            return Optional.empty();
        }
    }

    /**
     * Finds the user record by userId.
     *
     * @param userId userId of the record.
     * @return Optional&lt;UserAccessBean&gt;
     */
    @Override
    public Optional<UserAccessBean> findUserById(long userId) {
        return usersDAO.findByUserId(userId);
    }

    /**
     * Find user details by user emailId;
     *
     * @param emailId EmailId of the userId.
     * @return Optional&lt;UserAccessBean&gt;
     */
    @Override
    public Optional<UserAccessBean> findUserByEmailId(String emailId) {
        return usersDAO.findByUserEmailId(emailId);
    }

    /**
     * Find records of all users by userIds.
     *
     * @param userIds Collection of all userIds.
     * @return Optional&lt;List&lt;UserAccessBean&gt;&gt;
     */
    @Override
    public Optional<List<UserAccessBean>> findMultipleUsersById(Set<Long> userIds) {
        return usersDAO.findMultipleUsersByIds(userIds);
    }

    /**
     * Finds all user details by specified user role.
     *
     * @param userRole  The role for which user details are required.
     * @param maxResult The number of records to be pulled from DB, use
     *                  InventortConstants.DEFAULT_MAX_RESULTS and variants to
     *                  specify the limit.
     * @return Optional&lt;List&lt;UserAccessBean&gt;&gt;
     */
    @Override
    public Optional<List<UserAccessBean>> findByUserRole(String userRole, int maxResult) {
        if (maxResult == -1) {
            maxResult = Integer.MAX_VALUE;
        }
        return usersDAO.findByUserRole(userRole, maxResult);
    }

    /**
     * Disables an existing user account,either by userId or emailId and returns is
     * operation was successful.
     *
     * @param userId  The userId of account to be disabled
     * @param emailId The emailId of the account to be disabled.
     * @return boolean
     */
    @Override
    @Transactional
    public boolean disableUserAccount(Long userId, String emailId) {
        Optional<UserAccessBean> user;

        if (userId != null) {
            user = usersDAO.findByUserId(userId);
        } else if (emailId != null) {
            user = usersDAO.findByUserEmailId(emailId);
        } else {
            return InventoryConstants.OPERATIONFAILED;
        }

        user.ifPresent(userAccount -> userAccount.setAccountActive(false));
        return InventoryConstants.OPERATIONSUCCESSFUL;
    }

    /**
     * Changes user privileges either by userId or emailId and returns if operation
     * was successful.
     *
     * @param userId       userId of the user account
     * @param emailId      email of the user
     * @param isPrevilaged - Will make user as privileged if true.
     * @return boolean
     */
    @Override
    @Transactional
    public boolean changePrevileges(Long userId, String emailId, boolean isPrevilaged) {

        Optional<UserAccessBean> user;

        if (userId != null) {
            user = usersDAO.findByUserId(userId);
        } else if (emailId != null) {
            user = usersDAO.findByUserEmailId(emailId);
        } else {
            return InventoryConstants.OPERATIONFAILED;
        }

        user.ifPresent(userAccount -> {
            userAccount.setPrevilagedUser(isPrevilaged);
            LOGGER.info("User with userId: {} previlages changed to {} ", userAccount.getUserId(), isPrevilaged);
        });

        return InventoryConstants.OPERATIONSUCCESSFUL;
    }

    /**
     * Provides login service for the user and return true if authenticated.
     *
     * @param userDTO The user record with details to be authenticated.
     * @return boolean
     */
    @Override
    public boolean login(UserDTO userDTO) {
        UserAccessBean userAccessBean = modelMapper.map(userDTO, UserAccessBean.class);
        Optional<UserAccessBean> user = usersDAO.findByUserEmailId(userAccessBean.getEmailId());
        if (user.isPresent() && user.get().getPassword().equals(userAccessBean.getPassword())) {
            return InventoryConstants.OPERATIONSUCCESSFUL;
        }
        return InventoryConstants.OPERATIONFAILED;
    }

}
