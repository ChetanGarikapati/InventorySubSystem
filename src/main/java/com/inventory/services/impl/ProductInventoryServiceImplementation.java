package com.inventory.services.impl;

import com.inventory.constants.InventoryConstants;
import com.inventory.dao.ProductInventoryDAO;
import com.inventory.entities.ProductAccessBean;
import com.inventory.entities.ProductInventoryAccessBean;
import com.inventory.entities.WareHouseAccessBean;
import com.inventory.services.ProductInventoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * This is the base implementation class for
 *
 * @author Chetan Garikapati
 * <br>
 * Created On : 27-Oct-2018
 */
@Service(value = "BaseProductInventoryServiceImplementation")
public class ProductInventoryServiceImplementation implements ProductInventoryService {

    private ProductInventoryDAO productInventoryDAO;

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductInventoryServiceImplementation.class);

    @Autowired
    public ProductInventoryServiceImplementation(ProductInventoryDAO productInventoryDAO) {
        this.productInventoryDAO = productInventoryDAO;
    }

    public ProductInventoryAccessBean saveProductInventoryRecord(ProductInventoryAccessBean productInventoryAccessBean) {
        try {
            return productInventoryDAO.saveProductInventoryRecord(productInventoryAccessBean);
        } catch (Exception e) {
            LOGGER.error("Failed Saving ProductInventory Record : {} ", productInventoryAccessBean,e);
            return null;
        }
    }

    public Optional<ProductInventoryAccessBean> findProductInventoryByProductInventoryId(Long productInventoryId) {
        return productInventoryDAO.findProductInventoryByProductInventoryId(productInventoryId);
    }

    public Optional<ProductInventoryAccessBean> findInventoryByProductIdAndWareHouseId(ProductAccessBean productId,WareHouseAccessBean wareHouseId) {
        return productInventoryDAO.findInventoryByProductIdAndWareHouseId(productId,wareHouseId);
    }

    public Boolean deleteProductInventoryById(ProductInventoryAccessBean productInventoryAccessBean) {
        try {
            return productInventoryDAO.deleteProductInventoryRecord(productInventoryAccessBean);
        } catch (Exception e) {
            LOGGER.error("Failed Deleting ProductInventory Record : {} ", productInventoryAccessBean);
            return InventoryConstants.OPERATIONFAILED;
        }
    }

    public Boolean deleteProductInventoryByProductInventoryId(Long productInventoryId) {
        return deleteProductInventoryById(new ProductInventoryAccessBean(productInventoryId));
    }

    @Override
    public Optional<List<ProductInventoryAccessBean>> findInventoryByProductIdByWareHouseIds(ProductAccessBean productId, List<WareHouseAccessBean> wareHouseIds) {
        if(wareHouseIds.isEmpty()){
            return Optional.empty();
        }
        return productInventoryDAO.findInventoryByProductIdByWareHouseIds(productId,wareHouseIds);
    }
}
