package com.inventory.services.impl;

import com.inventory.constants.InventoryConstants;
import com.inventory.dao.ProductDAO;
import com.inventory.entities.ProductAccessBean;
import com.inventory.services.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * The base implementation class ProductService.
 *
 * @author Chetan Garikapati <br>
 * Created on : 20-Oct-2018
 */
@Service(value = "BaseProductServiceImplementation")
public class ProductServiceImplementation implements ProductService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceImplementation.class);
    private ProductDAO productDAO;

    @Autowired
    public ProductServiceImplementation(ProductDAO productDAO) {
        this.productDAO = productDAO;
    }

    /**
     * Saves new record or updates record.
     *
     * @param productAccessBean The record to be saved.
     * @return Optional&lt;ProductAccessBean&gt;
     */
    @Override
    public Optional<ProductAccessBean> saveOrUpdateProduct(ProductAccessBean productAccessBean) {
        try {
            return Optional.ofNullable(productDAO.saveOrUpdateProductRecord(productAccessBean));
        } catch (Exception e) {
            LOGGER.error("Failed saving product record {} ", e);
            return Optional.empty();
        }
    }

    /**
     * Finds Product details by productId.
     *
     * @param productId The productId of the Product.
     * @return Optional&lt;ProductAccessBean&gt;
     */
    @Override
    public Optional<ProductAccessBean> findByProductId(Long productId) {
        return productDAO.findByProductId(productId);
    }

    /**
     * Finds Product details by product partnumber.
     *
     * @param partNumber Partnumber of the product.
     * @return Optional&lt;List&lt;ProductAccessBean&gt;&gt;
     */
    @Override
    public Optional<List<ProductAccessBean>> findByPartNumber(String partNumber) {
        return productDAO.findByPartNumber(partNumber);
    }

    /**
     * Find all products by sellerId.
     *
     * @param sellerId The sellerId of the product seller.
     * @return Optional&lt;List&lt;ProductAccessBean&gt;&gt;
     */
    @Override
    public Optional<List<ProductAccessBean>> findProductBySellerId(Integer sellerId) {
        return productDAO.findProductsBySellerId(sellerId);
    }

    /**
     * Deletes the product record.
     *
     * @param productAccessBean The product record to be deleted.
     * @return boolean
     */
    @Override
    public boolean deleteProduct(ProductAccessBean productAccessBean) {
        try {
            return productDAO.removeProduct(productAccessBean);
        } catch (Exception e) {
            LOGGER.error("Failed deleting product record {} ", productAccessBean, e);
            return InventoryConstants.OPERATIONFAILED;
        }
    }

    /**
     * Deletes the product record.
     *
     * @param productId The productId of record to be deleted.
     * @return boolean
     */
    @Override
    public boolean deleteProduct(Long productId) {
        return deleteProduct(new ProductAccessBean(productId));
    }
}
