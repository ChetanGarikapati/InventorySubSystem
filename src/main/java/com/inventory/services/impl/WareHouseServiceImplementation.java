package com.inventory.services.impl;

import com.inventory.constants.InventoryConstants;
import com.inventory.dao.WareHouseDAO;
import com.inventory.entities.WareHouseAccessBean;
import com.inventory.services.WareHouseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * The base implementation class of WareHouseService.
 *
 * @author Chetan Garikapati <br>
 * Created on : 20-Oct-2018
 */
@Service(value = "BaseWareHouseServiceImplementation")
public class WareHouseServiceImplementation implements WareHouseService {

    private static final Logger LOGGER = LoggerFactory.getLogger(WareHouseServiceImplementation.class);
    private WareHouseDAO wareHouseDAO;

    @Autowired
    public WareHouseServiceImplementation(WareHouseDAO wareHouseDAO) {
        this.wareHouseDAO = wareHouseDAO;
    }

    /**
     * Find warehouse details by wareHouseId.
     *
     * @param wareHouseId wareHouseId of required warehouse.
     * @return Optional&lt;WareHouseAccessBean&gt;
     */
    @Override
    public Optional<WareHouseAccessBean> findByWareHouseId(Integer wareHouseId) {
        return wareHouseDAO.findByWareHouseId(wareHouseId);
    }

    /**
     * Find warehouse details by warehouse name.
     *
     * @param wareHouseName The name of the warehouse.
     * @return Optional&lt;WareHouseAccessBean&gt;
     */
    @Override
    public Optional<WareHouseAccessBean> findByWareHouseName(String wareHouseName) {
        return wareHouseDAO.findByWareHouseName(wareHouseName);
    }

    /**
     * Find all wareHouses belonging to sector by sectorId.
     *
     * @param sectorId   The sectorId of the wareHouse.
     * @param maxResults The number of records to be pulled from DB, use
     *                   InventortConstants.DEFAULT_MAX_RESULTS and variants to
     *                   specify the limit.
     * @return Optional&lt;List&lt;WareHouseAccessBean&gt;&gt;
     */
    @Override
    public Optional<List<WareHouseAccessBean>> findWareHousesBySectorId(Integer sectorId, Integer maxResults) {
        if (maxResults == null) {
            maxResults = InventoryConstants.DEFAULT_MAX_RESULTS;
            LOGGER.info("No value passed for maxResults, limiting result list to {} ",
                    InventoryConstants.DEFAULT_MAX_RESULTS);
        }
        return wareHouseDAO.findByWareHouseSector(sectorId, maxResults);
    }

    /**
     * Deletes an existing record.
     *
     * @param wareHouseAccessBean The record to be saved.
     * @return boolean
     */
    @Override
    public boolean deleteWareHouseRecord(WareHouseAccessBean wareHouseAccessBean) {
        try {
            return wareHouseDAO.deleteWareHouseRecord(wareHouseAccessBean);
        } catch (Exception e) {
            LOGGER.error("Deleting WareHouse Record {} failed ", wareHouseAccessBean);
            return InventoryConstants.OPERATIONFAILED;
        }
    }

    /**
     * Deletes an existing record.
     *
     * @param wareHouseId The id of record to be saved.
     * @return boolean
     */
    @Override
    public boolean deleteWareHouseRecord(Integer wareHouseId) {
        return deleteWareHouseRecord(new WareHouseAccessBean(wareHouseId));
    }

    /**
     * Saves new record or Updates existing records.
     *
     * @param wareHouseAccessBean The record to be saved.
     * @return Optional&lt;WareHouseAccessBean&gt;
     */
    @Override
    public Optional<WareHouseAccessBean> saveOrUpdateWareHouse(WareHouseAccessBean wareHouseAccessBean) {
        try {
            return Optional.ofNullable(wareHouseDAO.saveOrUpdateWareHouse(wareHouseAccessBean));
        } catch (Exception e) {
            LOGGER.error("Saving warehouse record {} failed ", wareHouseAccessBean);
            return Optional.empty();
        }
    }

}
