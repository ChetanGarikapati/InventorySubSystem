package com.inventory.services.impl;

import com.inventory.components.PlaceOrderFacade;
import com.inventory.dto.OrderDTO;
import com.inventory.dto.ProductRequestBean;
import com.inventory.services.PlaceOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This is the base implementation class for PlaceOrderService providing order inventory operations.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 9-Nov-2018
 */
@Service(value = "BasePlaceOrderService")
public class PlaceOrderServiceImplementation implements PlaceOrderService {

    private PlaceOrderFacade placeOrderFacade;

    @Autowired
    public PlaceOrderServiceImplementation(PlaceOrderFacade placeOrderFacade) {
        this.placeOrderFacade = placeOrderFacade;
    }

    @Override
    public OrderDTO placeOrder(ProductRequestBean productRequestBean) {
        return placeOrderFacade.holdOrder(productRequestBean);
    }

    @Override
    public OrderDTO confirmOrder(OrderDTO orderDTO) {
        return placeOrderFacade.confirmOrder(orderDTO);
    }

    @Override
    public OrderDTO cancelOrder(OrderDTO orderDTO) {
        return placeOrderFacade.cancelOrder(orderDTO);
    }
}
