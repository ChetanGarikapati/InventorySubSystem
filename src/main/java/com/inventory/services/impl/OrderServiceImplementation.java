package com.inventory.services.impl;

import com.inventory.constants.InventoryConstants;
import com.inventory.dao.OrderDAO;
import com.inventory.entities.OrderAccessBean;
import com.inventory.entities.OrderItemsAccessBean;
import com.inventory.services.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * The base implementation class for OrderService.
 * @author Chetan Garikapati
 * <br>
 * Created on : 3/11/18-Nov-2018
 */
@Service(value = "BaseOrderServiceImplementation")
public class OrderServiceImplementation implements OrderService {

    private OrderDAO orderDAO;

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderServiceImplementation.class);

    @Autowired
    public OrderServiceImplementation(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    @Override
    public Optional<OrderAccessBean> findOrderByOrderId(Long orderId) {
        return orderDAO.findOrderByOrderId(orderId);
    }

    @Override
    public OrderAccessBean saveOrUpdateOrder(OrderAccessBean orderAccessBean) {
        try {
            return orderDAO.saveOrUpdateOrder(orderAccessBean);
        } catch (Exception e) {
            LOGGER.error("Error saving order record : {} ", orderAccessBean, e);
            return null;
        }
    }

    @Override
    public OrderItemsAccessBean saveOrUpdateOrderItemForExistingProduct(OrderItemsAccessBean orderItemsAccessBean) {
        try {
            return orderDAO.saveOrUpdateOrderItemForExistingProduct(orderItemsAccessBean);
        } catch (Exception e) {
            LOGGER.error("Error saving orderItem record : {}", orderItemsAccessBean, e);
            return null;
        }
    }

    @Override
    public Boolean deleteOrder(OrderAccessBean orderAccessBean) {
        try {
            return orderDAO.deleteOrder(orderAccessBean);
        } catch (Exception e) {
            LOGGER.error("Error deleting order record : {} ", orderAccessBean, e);
            return InventoryConstants.OPERATIONFAILED;
        }
    }

    @Override
    public Optional<OrderItemsAccessBean> findOrderItemByOrderItemId(Long orderItemId) {
        return orderDAO.findOrderItemByOrderItemId(orderItemId);
    }

    @Override
    public List<OrderItemsAccessBean> saveMultipleOrderItemsForExistingOrders(List<OrderItemsAccessBean> orderItemsAccessBeans) {
        try {
            return orderDAO.saveMultipleOrderItemsForExistingOrders(orderItemsAccessBeans);
        } catch (Exception e) {
            LOGGER.error("Error saving multiple orderItems : {}", orderItemsAccessBeans, e);
            return null;
        }
    }

    @Override
    public Boolean deleteOrderItem(OrderItemsAccessBean orderItemsAccessBean) {
        try {
            return orderDAO.deleteOrderItem(orderItemsAccessBean);
        } catch (Exception e) {
            LOGGER.error("Error deleting orderItem record : {}", orderItemsAccessBean, e);
            return InventoryConstants.OPERATIONFAILED;
        }
    }

    public Boolean deleteOrderByOrderId(Long orderId) {
        return deleteOrder(new OrderAccessBean(orderId));
    }
}
