package com.inventory.services;

import com.inventory.entities.SectorWareHousePriorityAccessBean;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * The base template for Service Implementation class for providing
 * WareHousePriority operations in a Sector.
 *
 * @author Chetan Garikapati <br>
 * Created on : 20-Oct-2018
 */
public interface SectorWareHousePriorityService {

    /**
     * Find priority of all warehouses in a given sector.
     *
     * @param sectorId                         The sectorId for which priorities are
     *                                         required.
     * @param getWareHousesSortedByPriorityASC Specify if warehouses returned be
     *                                         sorted by priority in Ascending order
     * @return Optional&lt;List&lt;SectorWareHousePriorityAccessBean&gt;&gt;
     */
    Optional<List<SectorWareHousePriorityAccessBean>> findWareHousePriorityBySectorId(Integer sectorId,
                                                                                      Boolean getWareHousesSortedByPriorityASC);

    /**
     * Find warehouse priority for a given sector by sectorWareHousePriorityId.
     *
     * @param sectorWareHousePriorityId The sectorWareHousePriorityId for the
     *                                  record.
     * @return Optional&lt;SectorWareHousePriorityAccessBean&gt;
     */
    Optional<SectorWareHousePriorityAccessBean> findBySectorWareHousePriorityId(Integer sectorWareHousePriorityId);

    /**
     * Updates warehouse priority for a given sector and returns if update is
     * successful.
     *
     * @param sectorWareHousePriorityAccessBean THe record to be updated.
     * @return boolean
     */
    @Transactional
    boolean updateSectorWareHousePriority(SectorWareHousePriorityAccessBean sectorWareHousePriorityAccessBean);

    /**
     * Saves new record or updates existing record.
     *
     * @param sectorWareHousePriorityAccessBean The record to be saved.
     * @return Optional&lt;SectorWareHousePriorityAccessBean&gt;
     */
    Optional<SectorWareHousePriorityAccessBean> saveOrUpdateSectorWareHousePriority(
            SectorWareHousePriorityAccessBean sectorWareHousePriorityAccessBean);

    /**
     * Deletes an existing record.
     *
     * @param sectorWareHousePriorityAccessBean The record to be deleted.
     * @return boolean
     */
    boolean deleteSectorWareHousePriorityRecord(SectorWareHousePriorityAccessBean sectorWareHousePriorityAccessBean);

    /**
     * Deletes an existing record.
     *
     * @param sectorWareHousePriorityId The record id to be deleted.
     * @return boolean
     */
    boolean deleteSectorWareHousePriorityRecord(Integer sectorWareHousePriorityId);
}
