package com.inventory.services;

import com.inventory.dto.UserDTO;
import com.inventory.entities.UserAccessBean;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * The base template for Service Implementation class for providing User
 * operations.
 *
 * @author Chetan Garikapati <br>
 * Created on : 20-Oct-2018
 */
public interface UserServices {

    /**
     * Saves new user record or Updates existing records.
     *
     * @param userAccessBean The record to be saved.
     * @return Optional&lt;UserAccessBean&gt;
     */
    Optional<UserAccessBean> saveorUpdateUser(UserAccessBean userAccessBean);

    /**
     * Finds the user record by userId.
     *
     * @param userId userId of the record.
     * @return Optional&lt;UserAccessBean&gt;
     */
    Optional<UserAccessBean> findUserById(long userId);

    /**
     * Find user details by user emailId;
     *
     * @param emailId EmailId of the userId.
     * @return Optional&lt;UserAccessBean&gt;
     */
    Optional<UserAccessBean> findUserByEmailId(String emailId);

    /**
     * Find records of all users by userIds.
     *
     * @param userIds Collection of all userIds.
     * @return Optional&lt;List&lt;UserAccessBean&gt;&gt;
     */
    Optional<List<UserAccessBean>> findMultipleUsersById(Set<Long> userIds);

    /**
     * Finds all user details by specified user role.
     *
     * @param userRole  The role for which user details are required.
     * @param maxResult The number of records to be pulled from DB, use
     *                  InventortConstants.DEFAULT_MAX_RESULTS and variants to
     *                  specify the limit.
     * @return Optional&lt;List&lt;UserAccessBean&gt;&gt;
     */
    Optional<List<UserAccessBean>> findByUserRole(String userRole, int maxResult);

    /**
     * Disables an existing user account,either by userId or emailId and returns is
     * operation was successful.
     *
     * @param userId  The userId of account to be disabled
     * @param emailId The emailId of the account to be disabled.
     * @return boolean
     */
    @Transactional
    boolean disableUserAccount(Long userId, String emailId);

    /**
     * Changes user privileges either by userId or emailId and returns if operation was successful.
     *
     * @param userId       userId of the user account
     * @param emailId      email of the user
     * @param isPrevilaged - Will make user as privileged if true.
     * @return boolean
     */
    @Transactional
    boolean changePrevileges(Long userId, String emailId, boolean isPrevilaged);

    /**
     * Provides login service for the user and return true if authenticated.
     *
     * @param userDTO The user record with details to be authenticated.
     * @return boolean
     */
    boolean login(UserDTO userDTO);
}
