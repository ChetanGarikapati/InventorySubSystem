package com.inventory.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * The entity bean for accessing "ORDERS" Table.
 *
 * @author Chetan Garikapati <br>
 * Created on : 21-Oct-2018
 */
@Entity
@Table(name = "ORDERS")
public class OrderAccessBean implements Serializable {

    private static final long serialVersionUID = -2998081582438556428L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ORDER_ID", nullable = false)
    private Long orderId;

    @Column(name = "NUMBER_OF_ITEMS", nullable = false)
    private Integer numberOfItems;

    @Column(name = "DATE_OF_CREATION", nullable = false)
    private Instant dateOfCreation;

    @Column(name = "LAST_UPDATED", nullable = false)
    private Instant lastUpdated;

    @Column(name = "ORDER_STATUS", nullable = false)
    private String orderStatus;

    @Column(name = "USER_ID", nullable = true)
    private String userId;

    @Column(name = "INVENTORY_ALLOCATION_STATUS", nullable = false)
    private String inventoryAllocationStatus;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "ORDER_ID")
    private Set<OrderItemsAccessBean> orderItems = new HashSet<>();

    public OrderAccessBean() {
    }

    public OrderAccessBean(Long orderId) {
        this.orderId = orderId;
    }

    public OrderAccessBean(Long orderId, Integer numberOfItems, Instant dateOfCreation, Instant lastUpdated,
                           String orderStatus, String userId) {
        this.orderId = orderId;
        this.numberOfItems = numberOfItems;
        this.dateOfCreation = dateOfCreation;
        this.lastUpdated = lastUpdated;
        this.orderStatus = orderStatus;
        this.userId = userId;
    }

    public OrderAccessBean(Long orderId, Integer numberOfItems, Instant dateOfCreation, Instant lastUpdated, String orderStatus, String userId, String inventoryAllocationStatus, Set<OrderItemsAccessBean> orderItems) {
        this.orderId = orderId;
        this.numberOfItems = numberOfItems;
        this.dateOfCreation = dateOfCreation;
        this.lastUpdated = lastUpdated;
        this.orderStatus = orderStatus;
        this.userId = userId;
        this.inventoryAllocationStatus = inventoryAllocationStatus;
        this.orderItems = orderItems;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Integer getNumberOfItems() {
        return numberOfItems;
    }

    public void setNumberOfItems(Integer numberOfItems) {
        this.numberOfItems = numberOfItems;
    }

    public Instant getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(Instant dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public Instant getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Instant lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Set<OrderItemsAccessBean> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(Set<OrderItemsAccessBean> orderItems) {
        this.orderItems = orderItems;
    }

    public void addOrderItem(OrderItemsAccessBean orderItemsAccessBean) {
        this.orderItems.add(orderItemsAccessBean);
    }

    public void removeOrderItem(OrderItemsAccessBean orderItemsAccessBean) {
        this.orderItems.remove(orderItemsAccessBean);
    }

    public String getInventoryAllocationStatus() {
        return inventoryAllocationStatus;
    }

    public void setInventoryAllocationStatus(String inventoryAllocationStatus) {
        this.inventoryAllocationStatus = inventoryAllocationStatus;
    }

    @Override
    public String toString() {
        return "OrderAccessBean{" +
                "orderId=" + orderId +
                ", numberOfItems=" + numberOfItems +
                ", dateOfCreation=" + dateOfCreation +
                ", lastUpdated=" + lastUpdated +
                ", orderStatus='" + orderStatus + '\'' +
                ", userId='" + userId + '\'' +
                ", inventoryAllocationStatus='" + inventoryAllocationStatus + '\'' +
                ", orderItems=" + orderItems +
                '}';
    }
}
