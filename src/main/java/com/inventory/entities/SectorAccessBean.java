package com.inventory.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The entity bean for accessing "SECTOR" Table.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 20-Oct-2018
 */
@Entity
@Table(name = "SECTOR")
@NamedQuery(name = "SECTOR.findAllByPriority", query = "SELECT s FROM SectorAccessBean s WHERE s.priority = :priority")
@NamedQuery(name = "SECTOR.findAllBySortedPriority", query = "SELECT s FROM SectorAccessBean s")
public class SectorAccessBean implements Serializable {

    private static final long serialVersionUID = -4547801868394401974L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SECTOR_ID")
    private Integer sectorId;

    @Column(name = "SECTOR_PRIORITY", nullable = false)
    private Integer priority;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "SECTOR_IDENTIFYING_ADDRESS_ID", referencedColumnName = "ADDRESS_ID", foreignKey = @ForeignKey(name = "FK_SECTOR_REGION"))
    private AddressAccessBean sectorIdentificationArea;

    public SectorAccessBean() {
        this.priority = 5;
    }

    public SectorAccessBean(Integer sectorId) {
        this();
        this.sectorId = sectorId;
    }

    public SectorAccessBean(Integer sectorId, Integer priority) {
        this.sectorId = sectorId;
        this.priority = priority;
    }

    public SectorAccessBean(Integer sectorId, Integer priority, AddressAccessBean sectorIdentificationArea) {
        this.sectorId = sectorId;
        this.priority = priority;
        this.sectorIdentificationArea = sectorIdentificationArea;
    }

    public Integer getSectorId() {
        return sectorId;
    }

    public void setSectorId(Integer sectorId) {
        this.sectorId = sectorId;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }


    public AddressAccessBean getSectorIdentificationArea() {
        return sectorIdentificationArea;
    }

    public void setSectorIdentificationArea(AddressAccessBean sectorIdentificationArea) {
        this.sectorIdentificationArea = sectorIdentificationArea;
    }

    @Override
    public String toString() {
        return "SectorAccessBean [sectorId=" + sectorId + ", priority=" + priority + ", sectorIdentificationArea="
                + sectorIdentificationArea + "]";
    }


}
