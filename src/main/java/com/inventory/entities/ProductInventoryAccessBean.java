package com.inventory.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The entity bean for accessing "PRODUCT_INVENTORY" Table.
 *
 * @author Chetan Garikapati <br>
 * Created on : 21-Oct-2018
 */
@Entity
@Table(name = "PRODUCT_INVENTORY", uniqueConstraints = @UniqueConstraint(name = "UK_PRODUCT_WHOUSE", columnNames = {
        "PRODUCT_ID", "WAREHOUSE_ID"}))
@NamedQuery(name = "PRODUCT_INVENTORY.findInventoryAvailabiltyForSector", query = "SELECT pi from ProductInventoryAccessBean pi WHERE pi.productId = :productId AND pi.wareHouseId IN :wareHouseIdList AND pi.availableQTY > 0")
@NamedQuery(name = "PRODUCT_INVENTORY.findInventoryInWareHouseByWareHouseId", query = "SELECT pi FROM ProductInventoryAccessBean pi WHERE pi.wareHouseId= :wareHouseId AND pi.productId = :productId")
public class ProductInventoryAccessBean implements Serializable {

    private static final long serialVersionUID = 2947890007731861927L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PRODUCT_INVENTORY_ID", nullable = false)
    private Long productInventoryId;

    @OneToOne(targetEntity = ProductAccessBean.class,fetch = FetchType.EAGER)
    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID", foreignKey = @ForeignKey(name = "FK_PRODINV_PRODID"), nullable = false)
    private ProductAccessBean productId;

    @OneToOne(targetEntity = WareHouseAccessBean.class,fetch = FetchType.EAGER)
    @JoinColumn(name = "WAREHOUSE_ID", referencedColumnName = "WAREHOUSE_ID", foreignKey = @ForeignKey(name = "FK_PRODINV_WHOUSE"), nullable = false)
    private WareHouseAccessBean wareHouseId;

    @Column(name = "AVAILABLE_QTY", nullable = false)
    private Integer availableQTY;

    public ProductInventoryAccessBean() {
    }

    public ProductInventoryAccessBean(Long productInventoryId) {
        this.productInventoryId = productInventoryId;
    }

    public ProductInventoryAccessBean(ProductAccessBean productId, WareHouseAccessBean wareHouseId) {
        this.productId = productId;
        this.wareHouseId = wareHouseId;
    }

    public ProductInventoryAccessBean(Long productInventoryId, ProductAccessBean productId, WareHouseAccessBean wareHouseId, Integer availableQTY) {
        this.productInventoryId = productInventoryId;
        this.productId = productId;
        this.wareHouseId = wareHouseId;
        this.availableQTY = availableQTY;
    }

    public Long getProductInventoryId() {
        return productInventoryId;
    }

    public void setProductInventoryId(Long productInventoryId) {
        this.productInventoryId = productInventoryId;
    }

    public ProductAccessBean getProductId() {
        return productId;
    }

    public void setProductId(ProductAccessBean productId) {
        this.productId = productId;
    }

    public WareHouseAccessBean getWareHouseId() {
        return wareHouseId;
    }

    public void setWareHouseId(WareHouseAccessBean wareHouseId) {
        this.wareHouseId = wareHouseId;
    }

    public Integer getAvailableQTY() {
        return availableQTY;
    }

    public void setAvailableQTY(Integer availableQTY) {
        this.availableQTY = availableQTY;
    }

    @Override
    public String toString() {
        return "ProductInventoryAccessBean [productInventoryId=" + productInventoryId + ", productId=" + productId
                + ", wareHouseId=" + wareHouseId + ", availableQTY=" + availableQTY + "]";
    }

}
