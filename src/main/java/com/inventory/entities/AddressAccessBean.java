package com.inventory.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The entity bean for accessing "ADDRESS" Table.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 20-Oct-2018
 */
@Entity
@Table(name = "ADDRESS")
public class AddressAccessBean implements Serializable {

    private static final long serialVersionUID = -567678977801729087L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ADDRESS_ID", nullable = false)
    private Integer addressId;

    @Column(name = "CITY", length = 500)
    private String city;

    @Column(name = "STATE", length = 500)
    private String state;

    @Column(name = "REGION", length = 500)
    private String region;

    @Column(name = "ZIPCODE", nullable = false)
    private Integer zipcode;

    public AddressAccessBean() {
    }

    public AddressAccessBean(Integer addressId) {
        this.addressId = addressId;
    }

    public AddressAccessBean(Integer addressId, String city, String state, String region, Integer zipcode) {
        this.addressId = addressId;
        this.city = city;
        this.state = state;
        this.region = region;
        this.zipcode = zipcode;
    }

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Integer getZipcode() {
        return zipcode;
    }

    public void setZipcode(Integer zipcode) {
        this.zipcode = zipcode;
    }

    @Override
    public String toString() {
        return "AddressAccessBean [addressId=" + addressId + ", city=" + city + ", state=" + state + ", region="
                + region + ", zipcode=" + zipcode + "]";
    }

}
