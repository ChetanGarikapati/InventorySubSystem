package com.inventory.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The entity bean for accessing "SECTOR_WAREHOUSE_PRIORITY" Table.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 20-Oct-2018
 */
@Entity
@Table(name = "SECTOR_WAREHOUSE_PRIORITY", uniqueConstraints = @UniqueConstraint(name = "UK_SECTOR_WHOUSE_PRIORITY", columnNames = {
        "SECTOR_ID", "WAREHOUSE_ID"}))
@NamedQuery(name = "SECTOR_WAREHOUSE_PRIORITY.findBySectorAndWareHouseId", query = "SELECT swp FROM SectorWareHousePriorityAccessBean swp WHERE swp.sectorId = :sectorId AND swp.wareHouseId = :wareHouseId")
public class SectorWareHousePriorityAccessBean implements Serializable {

    private static final long serialVersionUID = -5642180981324996033L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SECTOR_WHOUSE_PRIORITY_ID", nullable = false)
    private Integer sectorWareHousePriorityId;

    @OneToOne(targetEntity = SectorAccessBean.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "SECTOR_ID", referencedColumnName = "SECTOR_ID", foreignKey = @ForeignKey(name = "FK_SECTOR_ID"), nullable = false)
    private SectorAccessBean sectorId;

    @OneToOne(targetEntity = WareHouseAccessBean.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "WAREHOUSE_ID", referencedColumnName = "WAREHOUSE_ID", foreignKey = @ForeignKey(name = "FK_WHOUSE_ID"), nullable = false)
    private WareHouseAccessBean wareHouseId;

    @Column(name = "PRIORITY", nullable = false)
    private Integer priority;

    public SectorWareHousePriorityAccessBean() {
    }

    public SectorWareHousePriorityAccessBean(Integer sectorWareHousePriorityId){
        this.sectorWareHousePriorityId = sectorWareHousePriorityId;
    }

    public SectorWareHousePriorityAccessBean(SectorAccessBean sectorId) {
        this.sectorId = sectorId;
    }

    public SectorWareHousePriorityAccessBean(SectorAccessBean sectorId, WareHouseAccessBean wareHouseId) {
        this.sectorId = sectorId;
        this.wareHouseId = wareHouseId;
    }

    public SectorWareHousePriorityAccessBean(Integer sectorWareHousePriorityId, SectorAccessBean sectorId, WareHouseAccessBean wareHouseId,
                                             Integer priority) {
        this.sectorWareHousePriorityId = sectorWareHousePriorityId;
        this.sectorId = sectorId;
        this.wareHouseId = wareHouseId;
        this.priority = priority;
    }

    public Integer getSectorWareHousePriorityId() {
        return sectorWareHousePriorityId;
    }

    public void setSectorWareHousePriorityId(Integer sectorWareHousePriorityId) {
        this.sectorWareHousePriorityId = sectorWareHousePriorityId;
    }

    public SectorAccessBean getSectorId() {
        return sectorId;
    }

    public void setSectorId(SectorAccessBean sectorId) {
        this.sectorId = sectorId;
    }

    public WareHouseAccessBean getWareHouseId() {
        return wareHouseId;
    }

    public void setWareHouseId(WareHouseAccessBean wareHouseId) {
        this.wareHouseId = wareHouseId;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "SectorWareHousePriorityAccessBean{" +
                "sectorWareHousePriorityId=" + sectorWareHousePriorityId +
                ", sectorId=" + sectorId +
                ", wareHouseId=" + wareHouseId +
                ", priority=" + priority +
                '}';
    }
}
