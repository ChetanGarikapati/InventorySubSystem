package com.inventory.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The entity bean for accessing "PRODUCTS" Table.
 *
 * @author Chetan Garikapati <br>
 * Created on : 20-Oct-2018
 */
@Entity
@Table(name = "PRODUCTS", uniqueConstraints = @UniqueConstraint(name = "UK_PART_MANUFACTURER", columnNames = {
        "PART_NUMBER", "MANUFACTURER"}))
@NamedQuery(name = "PRODUCTS.findByPartNumber", query = "SELECT p FROM ProductAccessBean p WHERE p.partNumber = :partNumber")
@NamedQuery(name = "PRODUCTS.findProductsBySeller", query = "SELECT p FROM ProductAccessBean p WHERE p.sellerId = :sellerId")
public class ProductAccessBean implements Serializable {

    private static final long serialVersionUID = 1054975728806380682L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PRODUCT_ID", nullable = false)
    private Long productId;

    @Column(name = "PART_NUMBER", length = 500, nullable = false)
    private String partNumber;

    @Column(name = "MANUFACTURER", length = 500, nullable = false)
    private String manufacturer;

    @Column(name = "AVAILABLE_QTY", nullable = false)
    private Integer availableQty;

    @OneToOne(targetEntity = UserAccessBean.class)
    @JoinColumn(name = "SELLER_ID", referencedColumnName = "USER_ID", foreignKey = @ForeignKey(name = "FK_SELLER_USER"))
    private UserAccessBean sellerId;

    @Column(name = "ALERT_QTY", nullable = false)
    private Integer alertQty;

    @Column(name = "DROP_SHIP", nullable = false)
    private Boolean dropShip;

    public ProductAccessBean() {
    }

    public ProductAccessBean(Long productId) {
        this.productId = productId;
    }

    public ProductAccessBean(String partNumber) {
        this.partNumber = partNumber;
    }

    public ProductAccessBean(Long productId, String partNumber, String manufacturer, Integer availableQty,
                             UserAccessBean sellerId, Integer alertQty, Boolean dropShip) {
        this.productId = productId;
        this.partNumber = partNumber;
        this.manufacturer = manufacturer;
        this.availableQty = availableQty;
        this.sellerId = sellerId;
        this.alertQty = alertQty;
        this.dropShip = dropShip;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Integer getAvailableQty() {
        return availableQty;
    }

    public void setAvailableQty(Integer availableQty) {
        this.availableQty = availableQty;
    }

    public Integer getAlertQty() {
        return alertQty;
    }

    public void setAlertQty(Integer alertQty) {
        this.alertQty = alertQty;
    }

    public Boolean getDropShip() {
        return dropShip;
    }

    public void setDropShip(Boolean dropShip) {
        this.dropShip = dropShip;
    }

    public UserAccessBean getSellerId() {
        return sellerId;
    }

    public void setSellerId(UserAccessBean sellerId) {
        this.sellerId = sellerId;
    }

    @Override
    public String toString() {
        return "ProductAccessBean [productId=" + productId + ", partNumber=" + partNumber + ", manufacturer="
                + manufacturer + ", availableQty=" + availableQty + ", sellerId=" + sellerId.getUserId() + ", alertQty="
                + alertQty + ", dropShip=" + dropShip + "]";
    }

}
