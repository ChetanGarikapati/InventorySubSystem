package com.inventory.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The entity bean for accessing "WAREHOUSE" Table.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 20-Oct-2018
 */
@Entity
@Table(name = "WAREHOUSE", uniqueConstraints = @UniqueConstraint(name = "UK_WHOUSE_NAME", columnNames = {
        "WAREHOUSE_NAME"}))
@NamedQuery(name = "WAREHOUSE.findByWarehouseName", query = "SELECT w FROM WareHouseAccessBean w WHERE w.wareHouseName = :wareHouseName")
@NamedQuery(name = "WAREHOUSE.findAllBySector", query = "SELECT w FROM WareHouseAccessBean w WHERE w.wareHouseDefaultSectorId IN :wareHouseDefaultSectorId")
public class WareHouseAccessBean implements Serializable {

    private static final long serialVersionUID = 4520765108829597404L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "WAREHOUSE_ID", nullable = false)
    private Integer wareHouseId;

    @Column(name = "WAREHOUSE_NAME", nullable = false, length = 500)
    private String wareHouseName;

    @Column(name = "WAREHOUSE_SECTOR_ID", nullable = false)
    private Integer wareHouseDefaultSectorId;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ADDRESS_ID", foreignKey = @ForeignKey(name = "FK_ADDRESS"), nullable = false)
    private AddressAccessBean addressDetails;

    public WareHouseAccessBean() {
    }

    public WareHouseAccessBean(Integer wareHouseId) {
        this.wareHouseId = wareHouseId;
    }

    public WareHouseAccessBean(Integer wareHouseId, String wareHouseName, Integer wareHouseDefaultSectorId,
                               AddressAccessBean addressDetails) {
        this.wareHouseId = wareHouseId;
        this.wareHouseName = wareHouseName;
        this.wareHouseDefaultSectorId = wareHouseDefaultSectorId;
        this.addressDetails = addressDetails;
    }

    public Integer getWareHouseId() {
        return wareHouseId;
    }

    public void setWareHouseId(Integer wareHouseId) {
        this.wareHouseId = wareHouseId;
    }

    public String getWareHouseName() {
        return wareHouseName;
    }

    public void setWareHouseName(String wareHouseName) {
        this.wareHouseName = wareHouseName;
    }

    public Integer getWareHouseDefaultSectorId() {
        return wareHouseDefaultSectorId;
    }

    public void setWareHouseDefaultSectorId(Integer wareHouseDefaultSectorId) {
        this.wareHouseDefaultSectorId = wareHouseDefaultSectorId;
    }

    public AddressAccessBean getAddressDetails() {
        return addressDetails;
    }

    public void setAddressDetails(AddressAccessBean addressDetails) {
        this.addressDetails = addressDetails;
    }

    @Override
    public String toString() {
        return "WareHouseAccessBean [wareHouseId=" + wareHouseId + ", wareHouseName=" + wareHouseName
                + ", wareHouseDefaultSectorId=" + wareHouseDefaultSectorId + ", addressDetails=" + addressDetails + "]";
    }

}
