package com.inventory.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

/**
 * The entity bean for accessing "USERS" Table.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 20-Oct-2018
 */
@Entity
@Table(name = "USERS", uniqueConstraints = @UniqueConstraint(name = "UK_EMAIL", columnNames = {"EMAILID"}))
@NamedQuery(name = "USERS.findMultipleUsersById", query = "SELECT u FROM UserAccessBean u WHERE u.userId IN :userIds")
@NamedQuery(name = "USERS.findByEmailId", query = "SELECT u FROM UserAccessBean u WHERE u.emailId = :emailId")
@NamedQuery(name = "USERS.findByRole", query = "SELECT u FROM UserAccessBean u WHERE u.userRole = :userRole")
public class UserAccessBean implements Serializable {

    private static final long serialVersionUID = -8380038519795981951L;

    @Id
    @Column(name = "USER_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userId;

    @Column(name = "USERNAME", nullable = false)
    private String username;

    @Column(name = "EMAILID", nullable = false)
    private String emailId;

    @Column(name = "PREVILAGED_USER", nullable = false)
    private boolean previlagedUser;

    @Column(name = "DATE_OF_ACCOUNT_CREATION")
    private Instant dateOfCreation;

    @Column(name = "LAST_LOGIN")
    private Instant lastLogin;

    @Column(name = "IS_ACCOUNT_ACTIVE", nullable = false)
    private boolean accountActive;

    @Column(name = "PASSWORD", nullable = false, length = 700)
    private String password;

    @Column(name = "USER_ROLE", nullable = false)
    private String userRole;

    public UserAccessBean() {
    }

    public UserAccessBean(Integer userId) {
        this.userId = userId;
    }

    public UserAccessBean(String emailId) {
        this.emailId = emailId;
    }

    public UserAccessBean(Integer userId, String username, String emailId, boolean previlagedUser, Instant dateOfCreation,
                          boolean accountActive) {
        this.userId = userId;
        this.username = username;
        this.emailId = emailId;
        this.previlagedUser = previlagedUser;
        this.dateOfCreation = dateOfCreation;
        this.accountActive = accountActive;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public boolean isPrevilagedUser() {
        return previlagedUser;
    }

    public void setPrevilagedUser(boolean previlagedUser) {
        this.previlagedUser = previlagedUser;
    }

    public Instant getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(Instant dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public Instant getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Instant lastLogin) {
        this.lastLogin = lastLogin;
    }

    public boolean isAccountActive() {
        return accountActive;
    }

    public void setAccountActive(boolean accountActive) {
        this.accountActive = accountActive;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    @Override
    public String toString() {
        return "UserAccessBean [userId=" + userId + ", username=" + username + ", emailId=" + emailId
                + ", previlagedUser=" + previlagedUser + ", dateOfCreation=" + dateOfCreation + ", lastLogin="
                + lastLogin + ", accountActive=" + accountActive + ", userRole=" + userRole + "]";
    }

}
