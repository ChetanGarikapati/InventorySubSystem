package com.inventory.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

/**
 * The entity bean for accessing "ORDER_ITEMS" Table.
 *
 * @author Chetan Garikapati <br>
 * Created on : 21-Oct-2018
 */
@Entity
@Table(name = "ORDER_ITEMS")
public class OrderItemsAccessBean implements Serializable {

    private static final long serialVersionUID = -9032432919009738229L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ORDER_ITEM_ID", nullable = false)
    private Long orderItemId;

    @OneToOne(targetEntity = OrderAccessBean.class)
    @JoinColumn(name = "ORDER_ID", referencedColumnName = "ORDER_ID", foreignKey = @ForeignKey(name = "FK_ORDERITEM_ORDER"), nullable = false)
    private OrderAccessBean orderId;

    @OneToOne(targetEntity = ProductAccessBean.class)
    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID", foreignKey = @ForeignKey(name = "FK_ORDERITEM_PRODUCT"), nullable = false)
    private ProductAccessBean productId;

    @Column(name = "PRODUCT_QUANTITY", nullable = false)
    private Integer productQuantity;

    @Column(name = "ORDER_ITEM_STATUS", nullable = false)
    private String orderItemStatus;

    @Column(name = "INVENTORY_ALLOCATION_STATUS", nullable = false)
    private String inventoryAllocationStatus;

    @OneToOne(targetEntity = WareHouseAccessBean.class)
    @JoinColumn(name = "WAREHOUSE_ID", referencedColumnName = "WAREHOUSE_ID", nullable = false, foreignKey = @ForeignKey(name = "FK_ORDERITEM_FULFILL_WHOUSE"))
    private WareHouseAccessBean wareHouseId;

    @Column(name = "LAST_UPDATED")
    private Instant lastUpdated;

    public OrderItemsAccessBean() {
    }

    public OrderItemsAccessBean(Long orderItemId) {
        this.orderItemId = orderItemId;
    }

    public OrderItemsAccessBean(Long orderItemId, OrderAccessBean orderId, ProductAccessBean productId, Integer productQuantity, String orderItemStatus, WareHouseAccessBean wareHouseId) {
        this.orderItemId = orderItemId;
        this.orderId = orderId;
        this.productId = productId;
        this.productQuantity = productQuantity;
        this.orderItemStatus = orderItemStatus;
        this.wareHouseId = wareHouseId;
    }

    public Long getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Long orderItemId) {
        this.orderItemId = orderItemId;
    }

    public OrderAccessBean getOrderId() {
        return orderId;
    }

    public void setOrderId(OrderAccessBean orderId) {
        this.orderId = orderId;
    }

    public ProductAccessBean getProductId() {
        return productId;
    }

    public void setProductId(ProductAccessBean productId) {
        this.productId = productId;
    }

    public String getInventoryAllocationStatus() {
        return inventoryAllocationStatus;
    }

    public void setInventoryAllocationStatus(String inventoryAllocationStatus) {
        this.inventoryAllocationStatus = inventoryAllocationStatus;
    }

    public Integer getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(Integer productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getOrderItemStatus() {
        return orderItemStatus;
    }

    public void setOrderItemStatus(String orderItemStatus) {
        this.orderItemStatus = orderItemStatus;
    }

    public WareHouseAccessBean getWareHouseId() {
        return wareHouseId;
    }

    public void setWareHouseId(WareHouseAccessBean wareHouseId) {
        this.wareHouseId = wareHouseId;
    }

    public Instant getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Instant lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public String toString() {
        return "OrderItemsAccessBean{" +
                "orderItemId=" + orderItemId +
                ", orderId=" + orderId.getOrderId() +
                ", productId=" + productId.getProductId() +
                ", productQuantity=" + productQuantity +
                ", orderItemStatus='" + orderItemStatus + '\'' +
                ", inventoryAllocationStatus='" + inventoryAllocationStatus + '\'' +
                ", wareHouseId=" + wareHouseId.getWareHouseId() +
                ", lastUpdated=" + lastUpdated +
                '}';
    }
}
