package com.inventory.dao;

import com.inventory.entities.WareHouseAccessBean;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * The base template for DAO Implementation class for "WAREHOUSE" Table.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 20-Oct-2018
 */
public interface WareHouseDAO {

    /**
     * Find warehouse details by wareHouseId.
     *
     * @param wareHouseId wareHouseId of required warehouse.
     * @return Optional&lt;WareHouseAccessBean&gt;
     */
    Optional<WareHouseAccessBean> findByWareHouseId(Integer wareHouseId);

    /**
     * Find warehouse details by warehouse name.
     *
     * @param wareHouseName The name of the warehouse.
     * @return Optional&lt;WareHouseAccessBean&gt;
     */
    Optional<WareHouseAccessBean> findByWareHouseName(String wareHouseName);

    /**
     * Find all wareHouses belonging to sector by sectorId.
     *
     * @param wareHouseDefaultSectorId The sectorId of the wareHouse.
     * @param maxResults               The number of records to be pulled from DB, use InventortConstants.DEFAULT_MAX_RESULTS and variants to specify the limit.
     * @return Optional&lt;List&lt;WareHouseAccessBean&gt;&gt;
     */
    Optional<List<WareHouseAccessBean>> findByWareHouseSector(Integer wareHouseDefaultSectorId, Integer maxResults);

    /**
     * Saves new record or Updates existing records.
     *
     * @param wareHouseAccessBean The record to be saved.
     * @return WareHouseAccessBean
     */
    @Transactional
    WareHouseAccessBean saveOrUpdateWareHouse(WareHouseAccessBean wareHouseAccessBean);

    /**
     * Deletes an existing record.
     *
     * @param wareHouseAccessBean The record to be saved.
     * @return boolean
     */
    @Transactional
    boolean deleteWareHouseRecord(WareHouseAccessBean wareHouseAccessBean);

}
