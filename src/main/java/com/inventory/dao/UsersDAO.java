package com.inventory.dao;

import com.inventory.entities.UserAccessBean;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * This is the base template for DAO Implementation class for "USERS" Table.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 20-Oct-2018
 */
public interface UsersDAO {

    /**
     * Finds the user record by userId.
     *
     * @param userId userId of the record.
     * @return Optional&lt;UserAccessBean&gt;
     */
    Optional<UserAccessBean> findByUserId(Long userId);

    /**
     * Find records of all users by userIds.
     *
     * @param userIds Collection of all userIds.
     * @return Optional&lt;List&lt;UserAccessBean&gt;&gt;
     */
    Optional<List<UserAccessBean>> findMultipleUsersByIds(Set<Long> userIds);

    /**
     * Find user details by user emailId;
     *
     * @param emailId EmailId of the userId.
     * @return Optional&lt;UserAccessBean&gt;
     */
    Optional<UserAccessBean> findByUserEmailId(String emailId);

    /**
     * Finds all user details by specified user role.
     *
     * @param userRole  The role for which user details are required.
     * @param maxResult The number of records to be pulled from DB, use InventortConstants.DEFAULT_MAX_RESULTS and variants to specify the limit.
     * @return Optional&lt;List&lt;UserAccessBean&gt;&gt;
     */
    Optional<List<UserAccessBean>> findByUserRole(String userRole, int maxResult);

    /**
     * Saves new user record or Updates existing records.
     *
     * @param userAccessBean The record to be saved.
     * @return UserAccessBean
     */
    @Transactional
    UserAccessBean saveOrUpdateUser(UserAccessBean userAccessBean);

}
