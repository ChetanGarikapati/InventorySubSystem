package com.inventory.dao;

import com.inventory.entities.ProductAccessBean;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * This is the base template for creating DAO Implementation for "PRODUCTS" Table.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 20-Oct-2018
 */
public interface ProductDAO {

    /**
     * Finds Product details by productId.
     *
     * @param productId The productId of the Product.
     * @return Optional&lt;ProductAccessBean&gt;
     */
    Optional<ProductAccessBean> findByProductId(Long productId);

    /**
     * Saves new record or updates record and return if operation is successful.
     *
     * @param productAccessBean The record to be saved.
     * @return ProductAccessBean
     */
    @Transactional
    ProductAccessBean saveOrUpdateProductRecord(ProductAccessBean productAccessBean);

    /**
     * Finds Product details by product partnumber.
     *
     * @param partNumber Partnumber of the product.
     * @return Optional&lt;List&lt;ProductAccessBean&gt;&gt;
     */
    Optional<List<ProductAccessBean>> findByPartNumber(String partNumber);

    /**
     * Find all products by sellerId.
     *
     * @param sellerId The sellerId of the product seller.
     * @return Optional&lt;List&lt;ProductAccessBean&gt;&gt;
     */
    Optional<List<ProductAccessBean>> findProductsBySellerId(Integer sellerId);

    /**
     * Deletes the product record.
     *
     * @param productAccessBean The product record to be deleted.
     * @return boolean
     */
    @Transactional
    boolean removeProduct(ProductAccessBean productAccessBean);
}
