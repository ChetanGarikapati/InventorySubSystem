package com.inventory.dao;

import com.inventory.entities.SectorAccessBean;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * The base template for DAO Implementation class for "SETORS" Table.
 *
 * @author Chetan Garikapati <br>
 * Created on : 20-Oct-2018
 */
public interface SectorDAO {

    /**
     * Find records by sectorId.
     *
     * @param sectorId The sectorId of the record.
     * @return Optional&lt;SectorAccessBean&gt;
     */
    Optional<SectorAccessBean> findBySectorId(Integer sectorId);

    /**
     * Find list of sectors with specified priority.
     *
     * @param priority The priority for which sectors are required.
     * @return Optional&lt;List&lt;SectorAccessBean&gt;&gt;
     */
    Optional<List<SectorAccessBean>> findSectorsByPriority(Integer priority);

    /**
     * Find all sectors sorted by priorities.
     *
     * @param getSectorPriorityByASC if true the values are returned sorted by
     *                               priority in Ascending order else returns in
     *                               Descending order.
     * @return Optional&lt;List&lt;SectorAccessBean&gt;&gt;
     */
    Optional<List<SectorAccessBean>> findSectorsBySortedPriority(boolean getSectorPriorityByASC);

    /**
     * Deletes an existing sector record.
     *
     * @param sectorAccessBean The record to be deleted.
     * @return boolean
     */
    @Transactional
    boolean deleteSector(SectorAccessBean sectorAccessBean);

    /**
     * Saves new record or updates existing record.
     *
     * @param sectorAccessBean The record to be saved.
     * @return SectorAccessBean
     */
    @Transactional
    SectorAccessBean saveOrUpdateSector(SectorAccessBean sectorAccessBean);

}
