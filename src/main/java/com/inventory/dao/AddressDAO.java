package com.inventory.dao;

import com.inventory.entities.AddressAccessBean;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * This is base template for creating DAO Implementation class for "ADDRESS" table.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 20-Oct-2018
 */
public interface AddressDAO {

    /**
     * Find Address Record by addressId.
     *
     * @param addressId The id for expected record.
     * @return Optional&lt;AddressAccessBean&gt;
     */
    Optional<AddressAccessBean> findByAddressId(Integer addressId);

    /**
     * Save new record or update record and returns if record was saved.
     *
     * @param addressAccessBean The record value to be saved.
     * @return AddressAccessBean
     */
    @Transactional
    AddressAccessBean saveOrUpdateAddress(AddressAccessBean addressAccessBean);

    /**
     * Deletes an existing record and return if delete was successful.
     *
     * @param addressAccessBean The record to be deleted.
     * @return AddressAccessBean
     */
    @Transactional
    boolean deleteAddress(AddressAccessBean addressAccessBean);
}
