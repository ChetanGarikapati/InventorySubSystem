package com.inventory.dao;

import com.inventory.entities.SectorWareHousePriorityAccessBean;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * This is base template for creating DAO Implementation class for
 * "SECTOR_WAREHOUSE_PRIORITY" Table.
 *
 * @author Chetan Garikapati <br>
 * Created on : 20-Oct-2018
 */
public interface SectorWareHousePriorityDAO {

    /**
     * Find warehouse priority for a given sector by sectorWareHousePriorityId.
     *
     * @param sectorWareHousePriorityId The sectorWareHousePriorityId for the
     *                                  record.
     * @return Optional&lt;SectorWareHousePriorityAccessBean&gt;
     */
    Optional<SectorWareHousePriorityAccessBean> findBySectorWareHousePriorityId(
            Integer sectorWareHousePriorityId);

    /**
     * Find priority of all warehouses in a given sector.
     *
     * @param sectorId                         The sectorId for which priorities are
     *                                         required.
     * @param getWareHousesSortedByPriorityASC Specify if warehouses returned be
     *                                         sorted by priority in Ascending order
     * @return Optional&lt;List&lt;SectorWareHousePriorityAccessBean&gt;&gt;
     */
    Optional<List<SectorWareHousePriorityAccessBean>> findAllWareHousesBySectorId(Integer sectorId,
                                                                                  Boolean getWareHousesSortedByPriorityASC);

    /**
     * Find priority by sectorId and warehouseId.
     *
     * @param sectorId    The sectorId and wareHouseId required of priority.
     * @param wareHouseId
     * @return Optional&lt;SectorWareHousePriorityAccessBean&gt;
     */
    Optional<SectorWareHousePriorityAccessBean> findBySectorAndWareHouseId(Integer sectorId,
                                                                           Integer wareHouseId);

    /**
     * Saves new record or updates existing record.
     *
     * @param sectorWareHousePriorityAccessBean The record to be saved.
     * @return SectorWareHousePriorityAccessBean
     */
    @Transactional
    SectorWareHousePriorityAccessBean saveOrUpdateWareHousePriority(
            SectorWareHousePriorityAccessBean sectorWareHousePriorityAccessBean);

    /**
     * Deletes an existing record.
     *
     * @param sectorWareHousePriorityAccessBean The record to be deleted.
     * @return boolean
     */
    @Transactional
    boolean deleteSectorWareHousePriority(SectorWareHousePriorityAccessBean sectorWareHousePriorityAccessBean);

}
