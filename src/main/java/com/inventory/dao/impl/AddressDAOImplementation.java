package com.inventory.dao.impl;

import com.inventory.constants.InventoryConstants;
import com.inventory.dao.AddressDAO;
import com.inventory.entities.AddressAccessBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Optional;

/**
 * This is base implementation class for AddressDAO.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 20-Oct-2018
 */
@Repository(value = "BaseAddressDAOImplementation")
public class AddressDAOImplementation implements AddressDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(AddressDAOImplementation.class);
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Find Address Record by addressId.
     *
     * @param addressId The id for expected record.
     * @return Optional&lt;AddressAccessBean&gt;
     */
    @Override
    public Optional<AddressAccessBean> findByAddressId(Integer addressId) {
        return Optional.ofNullable(entityManager.find(AddressAccessBean.class, addressId));
    }

    /**
     * Save new record or update record and returns if record was saved.
     *
     * @param addressAccessBean The record value to be saved.
     * @return AddressAccessBean
     */
    @Override
    @Transactional
    public AddressAccessBean saveOrUpdateAddress(AddressAccessBean addressAccessBean) {
        LOGGER.info("Saving Address record {} ", addressAccessBean);
        return entityManager.merge(addressAccessBean);
    }

    /**
     * Deletes an existing record and return if delete was successful.
     *
     * @param addressAccessBean The record to be deleted.
     * @return boolean
     */
    @Override
    @Transactional
    public boolean deleteAddress(AddressAccessBean addressAccessBean) {
        Optional<AddressAccessBean> optionalAddressAccessBean = findByAddressId(addressAccessBean.getAddressId());
        if ((optionalAddressAccessBean.isPresent())) {
            LOGGER.info("Deleting address record {} ", optionalAddressAccessBean.get());
            entityManager.remove(optionalAddressAccessBean.get());
            return InventoryConstants.OPERATIONSUCCESSFUL;
        }
        return InventoryConstants.OPERATIONFAILED;
    }

}
