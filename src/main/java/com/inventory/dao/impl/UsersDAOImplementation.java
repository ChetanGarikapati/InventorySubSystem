package com.inventory.dao.impl;

import com.inventory.dao.UsersDAO;
import com.inventory.entities.UserAccessBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * This is base implementation class for UsersDAO.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 20-Oct-2018
 */
@Repository(value = "BaseUsersDAOImplementation")
public class UsersDAOImplementation implements UsersDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsersDAOImplementation.class);
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Finds the user record by userId.
     *
     * @param userId userId of the record.
     * @return Optional&lt;UserAccessBean&gt;
     */
    @Override
    public Optional<UserAccessBean> findByUserId(Long userId) {
        return Optional.ofNullable(entityManager.find(UserAccessBean.class, userId));
    }

    /**
     * Find records of all users by userIds.
     *
     * @param userIds Collection of all userIds.
     * @return Optional&lt;List&lt;UserAccessBean&gt;&gt;
     */
    @Override
    public Optional<List<UserAccessBean>> findMultipleUsersByIds(Set<Long> userIds) {
        return Optional.ofNullable(entityManager.createNamedQuery("USERS.findMultipleUsersById", UserAccessBean.class)
                .setParameter("userIds", userIds).getResultList());
    }


    /**
     * Find user details by user emailId;
     *
     * @param emailId EmailId of the userId.
     * @return Optional&lt;UserAccessBean&gt;
     */
    @Override
    public Optional<UserAccessBean> findByUserEmailId(String emailId) {
        return Optional.ofNullable(entityManager.createNamedQuery("USERS.findByEmailId", UserAccessBean.class)
                .setParameter("emailId", emailId).getSingleResult());
    }

    /**
     * Finds all user details by specified user role.
     *
     * @param userRole  The role for which user details are required.
     * @param maxResult The number of records to be pulled from DB, use InventortConstants.DEFAULT_MAX_RESULTS and variants to specify the limit.
     * @return Optional&lt;List&lt;UserAccessBean&gt;&gt;
     */
    @Override
    public Optional<List<UserAccessBean>> findByUserRole(String userRole, int maxResult) {
        return Optional.ofNullable(entityManager.createNamedQuery("USERS.findByRole", UserAccessBean.class)
                .setParameter("userRole", userRole).setMaxResults(maxResult).getResultList());
    }

    /**
     * Saves new user record or Updates existing records.
     *
     * @param userAccessBean The record to be saved.
     * @return UserAccessBean
     */
    @Override
    @Transactional
    public UserAccessBean saveOrUpdateUser(UserAccessBean userAccessBean) {
        LOGGER.info("Saving user Record {}", userAccessBean);
        return entityManager.merge(userAccessBean);
    }

}
