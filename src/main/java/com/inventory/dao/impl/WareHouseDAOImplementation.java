package com.inventory.dao.impl;

import com.inventory.constants.InventoryConstants;
import com.inventory.dao.WareHouseDAO;
import com.inventory.entities.WareHouseAccessBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

/**
 * The is base implementation class for WareHouseDAO.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 20-Oct-2018
 */
@Repository(value = "BaseWareHouseDAOImplementation")
public class WareHouseDAOImplementation implements WareHouseDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(WareHouseDAOImplementation.class);
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Find warehouse details by wareHouseId.
     *
     * @param wareHouseId wareHouseId of required warehouse.
     * @return Optional&lt;WareHouseAccessBean&gt;
     */
    @Override
    public Optional<WareHouseAccessBean> findByWareHouseId(Integer wareHouseId) {
        return Optional.ofNullable(entityManager.find(WareHouseAccessBean.class, wareHouseId));
    }

    /**
     * Find warehouse details by warehouse name.
     *
     * @param wareHouseName The name of the warehouse.
     * @return Optional&lt;WareHouseAccessBean&gt;
     */
    @Override
    public Optional<WareHouseAccessBean> findByWareHouseName(String wareHouseName) {
        return Optional
                .ofNullable(entityManager.createNamedQuery("WAREHOUSE.findByWarehouseName", WareHouseAccessBean.class)
                        .setParameter("wareHouseName", wareHouseName).getSingleResult());
    }

    /**
     * Find all wareHouses belonging to sector by sectorId.
     *
     * @param wareHouseDefaultSectorId The sectorId of the wareHouse.
     * @param maxResults               The number of records to be pulled from DB, use InventortConstants.DEFAULT_MAX_RESULTS and variants to specify the limit.
     * @return Optional&lt;List&lt;WareHouseAccessBean&gt;&gt;
     */
    @Override
    public Optional<List<WareHouseAccessBean>> findByWareHouseSector(Integer wareHouseDefaultSectorId,
                                                                     Integer maxResults) {
        return Optional.ofNullable(entityManager.createNamedQuery("WAREHOUSE.findAllBySector", WareHouseAccessBean.class)
                .setParameter("wareHouseDefaultSectorId", wareHouseDefaultSectorId).setMaxResults(maxResults)
                .getResultList());
    }

    /**
     * Saves new record or Updates existing records.
     *
     * @param wareHouseAccessBean - The record to be saved.
     * @return WareHouseAccessBean
     */
    @Transactional
    @Override
    public WareHouseAccessBean saveOrUpdateWareHouse(WareHouseAccessBean wareHouseAccessBean) {
        LOGGER.info("Saving WareHouse Record {} ", wareHouseAccessBean);
        return entityManager.merge(wareHouseAccessBean);
    }

    /**
     * Deletes an existing record.
     *
     * @param wareHouseAccessBean The record to be saved.
     * @return boolean
     */
    @Transactional
    @Override
    public boolean deleteWareHouseRecord(WareHouseAccessBean wareHouseAccessBean) {
        Optional<WareHouseAccessBean> optionalWareHouse = findByWareHouseId(wareHouseAccessBean.getWareHouseId());
        if (optionalWareHouse.isPresent()) {
            LOGGER.info("Deleting WareHouse Record {} ", wareHouseAccessBean);
            entityManager.remove(optionalWareHouse.get());
            return InventoryConstants.OPERATIONSUCCESSFUL;
        }
        return InventoryConstants.OPERATIONFAILED;
    }

}
