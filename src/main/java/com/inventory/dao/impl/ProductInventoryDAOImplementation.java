package com.inventory.dao.impl;

import com.inventory.constants.InventoryConstants;
import com.inventory.dao.ProductInventoryDAO;
import com.inventory.entities.ProductAccessBean;
import com.inventory.entities.ProductInventoryAccessBean;
import com.inventory.entities.WareHouseAccessBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

/**
 * This is the base DAO Implementation class for ProductInventoryDAO.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 27-Oct-2018
 */
@Repository(value = "BaseProductInventoryDAOImplementation")
public class ProductInventoryDAOImplementation implements ProductInventoryDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductInventoryDAOImplementation.class);

    @PersistenceContext
    private EntityManager entityManager;

    public Optional<ProductInventoryAccessBean> findProductInventoryByProductInventoryId(Long productInventoryId) {
        return Optional.ofNullable(entityManager.find(ProductInventoryAccessBean.class, productInventoryId));
    }

    public Optional<ProductInventoryAccessBean> findInventoryByProductIdAndWareHouseId(ProductAccessBean productId, WareHouseAccessBean wareHouseId) {
        return Optional.ofNullable(entityManager.createNamedQuery("PRODUCT_INVENTORY.findInventoryInWareHouseByWareHouseId", ProductInventoryAccessBean.class)
                .setParameter("wareHouseId", wareHouseId)
                .setParameter("productId", productId)
                .getSingleResult());
    }

    @Transactional
    public ProductInventoryAccessBean saveProductInventoryRecord(ProductInventoryAccessBean productInventoryAccessBean) {
        LOGGER.info("Saving ProductInventoryRecord : {} ", productInventoryAccessBean);
        return entityManager.merge(productInventoryAccessBean);
    }

    @Transactional
    public Boolean deleteProductInventoryRecord(ProductInventoryAccessBean productInventoryAccessBean) {
        Optional<ProductInventoryAccessBean> optionalProductInventoryAccessBean = findProductInventoryByProductInventoryId(productInventoryAccessBean.getProductInventoryId());
        if (optionalProductInventoryAccessBean.isPresent()) {
            LOGGER.info("Deleting ProductInventoryRecord : {} ", optionalProductInventoryAccessBean.get());
            entityManager.remove(optionalProductInventoryAccessBean.get());
            return InventoryConstants.OPERATIONSUCCESSFUL;
        }
        return InventoryConstants.OPERATIONFAILED;
    }

    public Optional<List<ProductInventoryAccessBean>> findInventoryByProductIdByWareHouseIds(ProductAccessBean productId,List<WareHouseAccessBean> wareHouseIds) {
        return Optional.ofNullable(entityManager.createNamedQuery("PRODUCT_INVENTORY.findInventoryAvailabiltyForSector", ProductInventoryAccessBean.class)
                .setParameter("productId", productId)
                .setParameter("wareHouseIdList", wareHouseIds)
                .getResultList());
    }
}
