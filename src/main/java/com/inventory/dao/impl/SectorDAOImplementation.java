package com.inventory.dao.impl;

import com.inventory.constants.InventoryConstants;
import com.inventory.dao.SectorDAO;
import com.inventory.entities.SectorAccessBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

/**
 * This is base implementation class for SectorDAO.
 *
 * @author Chetan Garikapati <br>
 * Created on : 20-Oct-2018
 */
@Repository(value = "BaseSectorDAOImplementation")
public class SectorDAOImplementation implements SectorDAO {

    private static final String PRIORITY = "priority";
    private static final Logger LOGGER = LoggerFactory.getLogger(SectorDAOImplementation.class);
    @PersistenceContext
    private EntityManager entityManager;
    private CriteriaBuilder sectorCriteriaBuilder;
    private CriteriaQuery<SectorAccessBean> sectorCriteriaQuery;
    private Root<SectorAccessBean> sector;

    /**
     * Find records by sectorId.
     *
     * @param sectorId The sectorId of the record.
     * @return Optional&lt;SectorAccessBean&gt;
     */
    @Override
    public Optional<SectorAccessBean> findBySectorId(Integer sectorId) {
        return Optional.ofNullable(entityManager.find(SectorAccessBean.class, sectorId));
    }

    /**
     * Find list of sectors with specified priority.
     *
     * @param priority The priority for which sectors are required.
     * @return Optional&lt;List&lt;SectorAccessBean&gt;&gt;
     */
    @Override
    public Optional<List<SectorAccessBean>> findSectorsByPriority(Integer priority) {
        return Optional.ofNullable(entityManager.createNamedQuery("SECTOR.findAllByPriority", SectorAccessBean.class)
                .setParameter(PRIORITY, priority).getResultList());
    }

    /**
     * Find all sectors sorted by priorities.
     *
     * @param getSectorPriorityByASC if true the values are returned sorted by
     *                               priority in Ascending order else returns in
     *                               Descending order.
     * @return Optional&lt;List&lt;SectorAccessBean&gt;&gt;
     */
    @Override
    public Optional<List<SectorAccessBean>> findSectorsBySortedPriority(boolean getSectorPriorityByASC) {
        sectorCriteriaQuery.select(sector);
        if (getSectorPriorityByASC) {
            sectorCriteriaQuery.orderBy(sectorCriteriaBuilder.asc(sector.get(PRIORITY)));
        } else {
            sectorCriteriaQuery.orderBy(sectorCriteriaBuilder.desc(sector.get(PRIORITY)));
        }
        return Optional.ofNullable(entityManager.createQuery(sectorCriteriaQuery).getResultList());

    }

    /**
     * Deletes an existing sector record.
     *
     * @param sectorAccessBean The record to be deleted.
     * @return boolean
     */
    @Override
    @Transactional
    public boolean deleteSector(SectorAccessBean sectorAccessBean) {
        Optional<SectorAccessBean> optionalSector = findBySectorId(sectorAccessBean.getSectorId());
        if (optionalSector.isPresent()) {
            LOGGER.info("Deleting Sector Record {} ", sectorAccessBean);
            entityManager.remove(optionalSector.get());
            return InventoryConstants.OPERATIONSUCCESSFUL;
        }
        return InventoryConstants.OPERATIONFAILED;
    }

    /**
     * Saves new record or updates existing record.
     *
     * @param sectorAccessBean The record to be saved.
     * @return SectorAccessBean
     */
    @Override
    @Transactional
    public SectorAccessBean saveOrUpdateSector(SectorAccessBean sectorAccessBean) {
        LOGGER.info("Saving Sector Record {} ", sectorAccessBean);
        return entityManager.merge(sectorAccessBean);
    }

    @PostConstruct
    private void completeCriteriaObjectSetup() {
        this.sectorCriteriaBuilder = this.entityManager.getCriteriaBuilder();
        this.sectorCriteriaQuery = this.sectorCriteriaBuilder.createQuery(SectorAccessBean.class);
        this.sector = this.sectorCriteriaQuery.from(SectorAccessBean.class);
    }

}
