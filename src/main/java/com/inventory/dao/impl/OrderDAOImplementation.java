package com.inventory.dao.impl;

import com.inventory.constants.InventoryConstants;
import com.inventory.dao.OrderDAO;
import com.inventory.entities.OrderAccessBean;
import com.inventory.entities.OrderItemsAccessBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * The BaseDAO Implementation class OrderDAO to access "ORDER" and "ORDER_ITEM" Tables.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 3/11/18-Nov-2018
 */
@Repository(value = "BaseOrderDAOImplementation")
public class OrderDAOImplementation implements OrderDAO {

    @PersistenceContext
    private EntityManager entityManager;

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderDAOImplementation.class);

    public Optional<OrderAccessBean> findOrderByOrderId(Long orderId) {
        return Optional.ofNullable(entityManager.find(OrderAccessBean.class, orderId));
    }

    @Transactional
    public OrderAccessBean saveOrUpdateOrder(OrderAccessBean orderAccessBean) {
        LOGGER.info("Saving order record : {}", orderAccessBean);
        return entityManager.merge(orderAccessBean);
    }

    @Transactional
    public OrderItemsAccessBean saveOrUpdateOrderItemForExistingProduct(OrderItemsAccessBean orderItemsAccessBean) {
        LOGGER.info("Saving orderItem record : {}", orderItemsAccessBean);
        return entityManager.merge(orderItemsAccessBean);
    }

    @Transactional
    public Boolean deleteOrder(OrderAccessBean orderAccessBean) {
        LOGGER.info("Deleting order record : {}", orderAccessBean);

        Optional<OrderAccessBean> optionalOrderAccessBean = findOrderByOrderId(orderAccessBean.getOrderId());
        if (optionalOrderAccessBean.isPresent()) {
            entityManager.remove(optionalOrderAccessBean.get());
            return InventoryConstants.OPERATIONSUCCESSFUL;
        }
        return InventoryConstants.OPERATIONFAILED;

    }

    public Optional<OrderItemsAccessBean> findOrderItemByOrderItemId(Long orderItemId) {
        return Optional.ofNullable(entityManager.find(OrderItemsAccessBean.class, orderItemId));
    }

    @Transactional
    public List<OrderItemsAccessBean> saveMultipleOrderItemsForExistingOrders(List<OrderItemsAccessBean> orderItemsAccessBeans) {
        LOGGER.info("Saving orderItem records : {}", orderItemsAccessBeans);
        ArrayList<OrderItemsAccessBean> itemsAccessBeans = new ArrayList<>();
        orderItemsAccessBeans.forEach(orderItem -> {
            itemsAccessBeans.add(entityManager.merge(orderItem));
        });

        return itemsAccessBeans;
    }

    @Transactional
    public Boolean deleteOrderItem(OrderItemsAccessBean orderItemsAccessBean) {
        LOGGER.info("Deleting orderitem record : {}", orderItemsAccessBean);
        Optional<OrderItemsAccessBean> optionalOrderItemsAccessBean = findOrderItemByOrderItemId(orderItemsAccessBean.getOrderItemId());
        if(optionalOrderItemsAccessBean.isPresent()){
            entityManager.remove(optionalOrderItemsAccessBean.get());
            return InventoryConstants.OPERATIONSUCCESSFUL;
        }
        return InventoryConstants.OPERATIONFAILED;
    }
}
