package com.inventory.dao.impl;

import com.inventory.constants.InventoryConstants;
import com.inventory.dao.ProductDAO;
import com.inventory.entities.ProductAccessBean;
import com.inventory.entities.UserAccessBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

/**
 * This is base implementation class for ProductDAO.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 20-Oct-2018
 */
@Repository(value = "BaseProductDAOImplementation")
public class ProductDAOImplementation implements ProductDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductDAOImplementation.class);
    
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Finds Product details by productId.
     *
     * @param productId The productId of the Product.
     * @return Optional&lt;ProductAccessBean&gt;
     */
    @Override
    public Optional<ProductAccessBean> findByProductId(Long productId) {
        return Optional.ofNullable(entityManager.find(ProductAccessBean.class, productId));
    }

    /**
     * Saves new record or updates record and return if operation is successful.
     *
     * @param productAccessBean The record to be saved.
     * @return ProductAccessBean
     */
    @Transactional
    @Override
    public ProductAccessBean saveOrUpdateProductRecord(ProductAccessBean productAccessBean) {
        LOGGER.info("Saving product {} ", productAccessBean);
        return entityManager.merge(productAccessBean);
    }

    /**
     * Finds Product details by product partnumber.
     *
     * @param partNumber Partnumber of the product.
     * @return Optional&lt;List&lt;ProductAccessBean&gt;&gt;
     */
    @Override
    public Optional<List<ProductAccessBean>> findByPartNumber(String partNumber) {
        return Optional.ofNullable(entityManager.createNamedQuery("PRODUCTS.findByPartNumber", ProductAccessBean.class)
                .setParameter("partNumber", partNumber).getResultList());
    }

    /**
     * Find all products by sellerId.
     *
     * @param sellerId The sellerId of the product seller.
     * @return Optional&lt;List&lt;ProductAccessBean&gt;&gt;
     */
    @Override
    public Optional<List<ProductAccessBean>> findProductsBySellerId(Integer sellerId) {
        return Optional.ofNullable(entityManager.createNamedQuery("PRODUCTS.findProductsBySeller", ProductAccessBean.class)
                .setParameter("sellerId", new UserAccessBean(sellerId)).getResultList());
    }

    /**
     * Deletes the product record.
     *
     * @param productAccessBean The product record to be deleted.
     * @return boolean
     */
    @Override
    @Transactional
    public boolean removeProduct(ProductAccessBean productAccessBean) {
        Optional<ProductAccessBean> optionalProduct = findByProductId(productAccessBean.getProductId());
        if (optionalProduct.isPresent()) {
            LOGGER.info("Deleteing product record {}", optionalProduct.get());
            entityManager.remove(optionalProduct.get());
            return InventoryConstants.OPERATIONSUCCESSFUL;
        }
        return InventoryConstants.OPERATIONFAILED;

    }
}
