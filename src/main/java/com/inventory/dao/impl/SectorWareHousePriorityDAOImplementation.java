package com.inventory.dao.impl;

import com.inventory.constants.InventoryConstants;
import com.inventory.dao.SectorWareHousePriorityDAO;
import com.inventory.entities.SectorAccessBean;
import com.inventory.entities.SectorWareHousePriorityAccessBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

/**
 * This is base implementation class for SectorWareHousePriorityDAO.
 *
 * @author Chetan Garikapati <br>
 * Created on : 20-Oct-2018
 */
@Repository(value = "BaseSectorWareHousePriorityDAOImplementation")
public class SectorWareHousePriorityDAOImplementation implements SectorWareHousePriorityDAO {

    private static final String PRIORITY = "priority";
    private static final Logger LOGGER = LoggerFactory.getLogger(SectorWareHousePriorityDAOImplementation.class);
    @PersistenceContext
    private EntityManager entityManager;
    private CriteriaBuilder sectorWareHousePriorityCriteriaBuilder;
    private CriteriaQuery<SectorWareHousePriorityAccessBean> sectorWareHousePriorityCriteriaQuery;
    private Root<SectorWareHousePriorityAccessBean> sectorWareHousePriority;

    /**
     * Find warehouse priority for a given sector by sectorWareHousePriorityId.
     *
     * @param sectorWareHousePriorityId The sectorWareHousePriorityId for the
     *                                  record.
     * @return Optional&lt;SectorWareHousePriorityAccessBean&gt;
     */
    @Override
    public Optional<SectorWareHousePriorityAccessBean> findBySectorWareHousePriorityId(
            Integer sectorWareHousePriorityId) {
        return Optional
                .ofNullable(entityManager.find(SectorWareHousePriorityAccessBean.class, sectorWareHousePriorityId));
    }

    /**
     * Find priority of all warehouses in a given sector.
     *
     * @param sectorId                         The sectorId for which priorities are
     *                                         required.
     * @param getWareHousesSortedByPriorityASC Specify if warehouses returned be
     *                                         sorted by priority in Ascending order
     * @return Optional&lt;List&lt;SectorWareHousePriorityAccessBean&gt;&gt;
     */
    @Override
    public Optional<List<SectorWareHousePriorityAccessBean>> findAllWareHousesBySectorId(Integer sectorId,
                                                                                         Boolean getWareHousesSortedByPriorityASC) {

        sectorWareHousePriorityCriteriaQuery.select(sectorWareHousePriority);
        sectorWareHousePriorityCriteriaQuery.where(sectorWareHousePriorityCriteriaBuilder.equal(sectorWareHousePriority.get("sectorId"),new SectorAccessBean(sectorId)));
        if (getWareHousesSortedByPriorityASC) {
            sectorWareHousePriorityCriteriaQuery
                    .orderBy(sectorWareHousePriorityCriteriaBuilder.asc(sectorWareHousePriority.get(PRIORITY)));
        } else {
            sectorWareHousePriorityCriteriaQuery
                    .orderBy(sectorWareHousePriorityCriteriaBuilder.desc(sectorWareHousePriority.get(PRIORITY)));
        }
        return Optional.ofNullable(entityManager.createQuery(sectorWareHousePriorityCriteriaQuery).getResultList());
    }

    /**
     * Find priority by sectorId and warehouseId.
     *
     * @param sectorId    The sectorId and wareHouseId required of priority.
     * @param wareHouseId
     * @return Optional&lt;SectorWareHousePriorityAccessBean&gt;
     */
    @Override
    public Optional<SectorWareHousePriorityAccessBean> findBySectorAndWareHouseId(Integer sectorId,
                                                                                  Integer wareHouseId) {
        return Optional.ofNullable(entityManager
                .createNamedQuery("SECTOR_WAREHOUSE_PRIORITY.findBySectorAndWareHouseId",
                        SectorWareHousePriorityAccessBean.class)
                .setParameter("sectorId", sectorId).setParameter("wareHouseId", wareHouseId).getSingleResult());

    }

    /**
     * Saves new record or updates existing record.
     *
     * @param sectorWareHousePriorityAccessBean The record to be saved.
     * @return SectorWareHousePriorityAccessBean
     */
    @Override
    @Transactional
    public SectorWareHousePriorityAccessBean saveOrUpdateWareHousePriority(
            SectorWareHousePriorityAccessBean sectorWareHousePriorityAccessBean) {
        LOGGER.info("Saving saveOrUpdateWareHousePriority Record {} ", sectorWareHousePriorityAccessBean);
        return entityManager.merge(sectorWareHousePriorityAccessBean);
    }

    /**
     * Deletes an existing record.
     *
     * @param sectorWareHousePriorityAccessBean The record to be deleted.
     * @return boolean
     */
    @Override
    @Transactional
    public boolean deleteSectorWareHousePriority(SectorWareHousePriorityAccessBean sectorWareHousePriorityAccessBean) {
        Optional<SectorWareHousePriorityAccessBean> optionalSectorWareHousePriority = findBySectorWareHousePriorityId(
                sectorWareHousePriorityAccessBean.getSectorWareHousePriorityId());
        if (optionalSectorWareHousePriority.isPresent()) {
            LOGGER.error("Deleting Record {} ", sectorWareHousePriorityAccessBean);
            entityManager.remove(optionalSectorWareHousePriority.get());
            return InventoryConstants.OPERATIONSUCCESSFUL;
        }
        return InventoryConstants.OPERATIONFAILED;

    }

    @PostConstruct
    private void setupCriteriaBuilders() {
        this.sectorWareHousePriorityCriteriaBuilder = this.entityManager.getCriteriaBuilder();
        this.sectorWareHousePriorityCriteriaQuery = this.sectorWareHousePriorityCriteriaBuilder
                .createQuery(SectorWareHousePriorityAccessBean.class);
        this.sectorWareHousePriority = this.sectorWareHousePriorityCriteriaQuery
                .from(SectorWareHousePriorityAccessBean.class);
    }

}
