package com.inventory.dao;

import com.inventory.entities.ProductAccessBean;
import com.inventory.entities.ProductInventoryAccessBean;
import com.inventory.entities.WareHouseAccessBean;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * This is  base template for DAO Implementation class for accessing "PRODUCT_INVENTORY" Table.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 27-Oct-2018
 */
public interface ProductInventoryDAO {

    /**
     * Find ProductInventory record by productInventoryId.
     *
     * @param productInventoryId
     * @return Optional&lt;ProductInventoryAccessBean&gt;
     */
    public Optional<ProductInventoryAccessBean> findProductInventoryByProductInventoryId(Long productInventoryId);

    /**
     * Find Inventory of product by productId and wareHouseId.
     *
     * @param wareHouseId
     * @param productId
     * @return Optional&lt;ProductInventoryAccessBean&gt;
     */
    public Optional<ProductInventoryAccessBean> findInventoryByProductIdAndWareHouseId(ProductAccessBean productId, WareHouseAccessBean wareHouseId);

    /**
     * Save a new ProductInventory record.
     *
     * @param productInventoryAccessBean
     * @return ProductInventoryAccessBean
     */
    @Transactional
    public ProductInventoryAccessBean saveProductInventoryRecord(ProductInventoryAccessBean productInventoryAccessBean);

    /**
     * Delete a record from ProductInventory.
     *
     * @param productInventoryAccessBean
     * @return Boolean
     */
    @Transactional
    public Boolean deleteProductInventoryRecord(ProductInventoryAccessBean productInventoryAccessBean);

    /**
     * Provides list of warehouses containing inventory with available quantity of given productId.
     *
     * @param productId
     * @param wareHouseIds
     * @return Optional&lt;List&lt;ProductInventoryAccessBean&gt;&gt;
     */
    public Optional<List<ProductInventoryAccessBean>> findInventoryByProductIdByWareHouseIds(ProductAccessBean productId, List<WareHouseAccessBean> wareHouseIds);
}
