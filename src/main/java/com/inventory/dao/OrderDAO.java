package com.inventory.dao;

import com.inventory.entities.OrderAccessBean;
import com.inventory.entities.OrderItemsAccessBean;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @author Chetan Garikapati
 * <br>
 * Created on : 3/11/18-Nov-2018
 */
public interface OrderDAO {

    public Optional<OrderAccessBean> findOrderByOrderId(Long orderId);

    @Transactional
    public OrderAccessBean saveOrUpdateOrder(OrderAccessBean orderAccessBean);

    @Transactional
    public OrderItemsAccessBean saveOrUpdateOrderItemForExistingProduct(OrderItemsAccessBean orderItemsAccessBean);

    @Transactional
    public Boolean deleteOrder(OrderAccessBean orderAccessBean);

    public Optional<OrderItemsAccessBean> findOrderItemByOrderItemId(Long orderItemId);

    @Transactional
    public List<OrderItemsAccessBean> saveMultipleOrderItemsForExistingOrders(List<OrderItemsAccessBean> orderItemsAccessBeans);

    @Transactional
    public Boolean deleteOrderItem(OrderItemsAccessBean orderItemsAccessBean);
}
