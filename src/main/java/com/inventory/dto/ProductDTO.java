package com.inventory.dto;

/**
 * @author Chetan Garikapati
 * <br>
 * Created on : 9/11/18-Nov-2018
 */
public class ProductDTO {

    private Long productId;
    private Integer productQuantity;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(Integer productQuantity) {
        this.productQuantity = productQuantity;
    }

    @Override
    public String toString() {
        return "ProductDTO{" +
                "productId=" + productId +
                ", productQuantity=" + productQuantity +
                '}';
    }
}
