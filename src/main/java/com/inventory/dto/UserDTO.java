package com.inventory.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.Instant;

public class UserDTO {

    private long userId;
    private String username;
    private String emailId;
    private boolean previlagedUser;
    private Instant dateOfCreation;
    private Instant lastLogin;
    private boolean accountActive;
    private String userRole;
    private String password;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public boolean isPrevilagedUser() {
        return previlagedUser;
    }

    public void setPrevilagedUser(boolean previlagedUser) {
        this.previlagedUser = previlagedUser;
    }

    public Instant getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(Instant dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public Instant getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Instant lastLogin) {
        this.lastLogin = lastLogin;
    }

    public boolean isAccountActive() {
        return accountActive;
    }

    public void setAccountActive(boolean accountActive) {
        this.accountActive = accountActive;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "UserDTO [userId=" + userId + ", username=" + username + ", emailId=" + emailId + ", previlagedUser="
                + previlagedUser + ", dateOfCreation=" + dateOfCreation + ", lastLogin=" + lastLogin
                + ", accountActive=" + accountActive + ", userRole=" + userRole + "]";
    }

}
