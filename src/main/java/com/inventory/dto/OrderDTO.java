package com.inventory.dto;

import java.util.HashSet;

/**
 * @author Chetan Garikapati
 * <br>
 * Created on : 9/11/18-Nov-2018
 */
public class OrderDTO {

    private Long orderId;
    private String orderStatus;
    private HashSet<Long> outOfStockProducts = new HashSet<>();

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public HashSet<Long> getOutOfStockProducts() {
        return outOfStockProducts;
    }

    public void setOutOfStockProducts(HashSet<Long> outOfStockProducts) {
        this.outOfStockProducts = outOfStockProducts;
    }

    public void addOutOfStockProduct(Long productId){
        outOfStockProducts.add(productId);
    }

    public void removeOutOfStockProduct(Long productId){
        outOfStockProducts.remove(productId);
    }

    @Override
    public String toString() {
        return "OrderDTO{" +
                "orderId=" + orderId +
                ", orderStatus='" + orderStatus + '\'' +
                ", outOfStockProducts=" + outOfStockProducts +
                '}';
    }
}
