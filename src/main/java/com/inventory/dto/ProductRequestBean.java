package com.inventory.dto;

import com.inventory.entities.AddressAccessBean;

import java.util.ArrayList;

/**
 * @author Chetan Garikapati
 * <br>
 * Created on : 9/11/18-Nov-2018
 */
public class ProductRequestBean {

    private ArrayList<ProductDTO> productDetails;

    private AddressAccessBean deliveryLocation;

    public ArrayList<ProductDTO> getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(ArrayList<ProductDTO> productDetails) {
        this.productDetails = productDetails;
    }

    public AddressAccessBean getDeliveryLocation() {
        return deliveryLocation;
    }

    public void setDeliveryLocation(AddressAccessBean deliveryLocation) {
        this.deliveryLocation = deliveryLocation;
    }

    @Override
    public String toString() {
        return "ProductRequestBean{" +
                "productDetails=" + productDetails +
                ", deliveryLocation=" + deliveryLocation +
                '}';
    }
}
