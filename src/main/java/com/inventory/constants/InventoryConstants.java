package com.inventory.constants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class contains all the constant values used through out the application.
 *
 * @author Chetan Garikapati <br>
 * Created on : 20-Oct-2018
 */
public final class InventoryConstants {

    /**
     * GENERAL FIELDS.
     */
    public static final String SAVESUCCESSFUL = "SAVED RECORD";
    public static final String SAVEFAILED = "SAVING RECORD FAILED";
    public static final String INVALIDCREDENTIALS = "INVALID USER CREDENTIALS";
    public static final String NONPREVILAGEDUSER = "USER DOES NOT HAVE PREVILEGES TO PERFORM THIS OPERATION";
    public static final Boolean OPERATIONSUCCESSFUL = true;
    public static final Boolean OPERATIONFAILED = false;
    public static final String LOGINSUCCESSFUL = "USER LOGIN SUCCESSFUL";
    /**
     * DATA RETRIVAL LIMIT FIELDS.
     */
    public static final Integer DEFAULT_MAX_RESULTS = 100;
    public static final Integer DEFAULT_MAX_RESULTS_EXTENDED = 500;
    public static final Integer DEFAULT_MAX_RESULTS_EXTENDED_LARGE = 1000;
    /**
     * INVENTORY STATUS FOR ORDER ITEMS.
     */
    public static final String HOLD_INVENTORY = "INVENTORY_ON_HOLD";
    public static final String ALLOCATE_INVENTORY = "INVENTORY_ALLOCATED";
    public static final String DEALLOCATE_INVENTORY = "INVENTORY_DEALLOCATED";
    public static final String RETURN_INVENTORY_IN_PROGRESS = "INVENTORY_RETURN_IN_PROGRESS";
    public static final String RETURN_INVENTORY_COMPLETE = "INVENTORY_RETURNED";
    /**
     * ORDER STATUS VALUES.
     */
    public static final String ORDER_PENDING = "PENDING";
    public static final String ORDER_CONFIRMED = "CONFIRMED";
    public static final String ORDER_CANCELLED = "CANCELLED";
    public static final String ORDER_EXPIRED = "EXPIRED";
    public static final String ORDER_HOLD_FAIL = "ORDER HOLD FAIL";
    public static final String ORDER_ITEMS_OUT_OF_STOCK = "ALL PRODUCTS OUT OF STOCK";
    public static final String ORDER_NOT_IN_PENDING_STATE = "ORDER IS NOT IN PENDING_STATE";
    public static final String ORDER_DETAILS_INVALID = "INVALID ORDER DETAILS";
    public static final String ORDER_NOT_IN_CANCELLABLE_STATE = "ORDER IS NOT IN PENDING OR CONFIRMED STATE";
    public static final String ORDER_CANCELLATION_FAILED = "ORDER_CANCELLATION_FAILED";

    /**
     * INVENTORY ALLOCATION STRATERGY.
     */
    public static final StringBuffer RESOLVE_BY_CITY = new StringBuffer("CITY");
    public static final StringBuffer RESOLVE_BY_STATE = new StringBuffer("STATE");
    public static final StringBuffer RESOLVE_BY_REGION = new StringBuffer("REGION");
    public static final StringBuffer RESOLUTION_FAILED = new StringBuffer("RESOLUTION FAILED");
    public static final StringBuffer GLOBAL_SECTOR_FALLBACK = new StringBuffer("INVENTORY AVAILABLE BUT NOT IN ELIGIBLE SECTORS");

    private static final Logger LOGGER = LoggerFactory.getLogger(InventoryConstants.class);

    private InventoryConstants() {
        LOGGER.warn("THIS CLASS INSTANCE SHOULD NOT BE CREATED IT ONLY STORES CONSTANTS FOR INVENTORY APPLICATION");
    }

}
