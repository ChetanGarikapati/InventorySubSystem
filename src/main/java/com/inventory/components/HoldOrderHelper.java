package com.inventory.components;

import com.inventory.dto.OrderDTO;
import com.inventory.dto.ProductRequestBean;

/**
 * This is the base template for class providing hold product inventory for an order.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 10/11/18-Nov-2018
 */
public interface HoldOrderHelper {

    /**
     * This will perform the hold inventory operation and returns the order info of the current request.
     * @param productRequestBean Product items of the order
     * @return OrderDTO Contains info on current request
     */
    public OrderDTO processOrderRequest(ProductRequestBean productRequestBean);
}
