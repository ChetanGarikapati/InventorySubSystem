package com.inventory.components.impl;

import com.inventory.components.HoldOrderHelper;
import com.inventory.constants.InventoryConstants;
import com.inventory.dto.OrderDTO;
import com.inventory.dto.ProductDTO;
import com.inventory.dto.ProductRequestBean;
import com.inventory.entities.*;
import com.inventory.services.OrderService;
import com.inventory.services.ProductInventoryService;
import com.inventory.services.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * This is the base implementation class of HoldOrderHelper for holding the inventory for an order.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 9-Nov-2018
 */

@Component(value = "BaseHoldOrderHelperImplementation")
public class HoldOrderHelperImplementation implements HoldOrderHelper {

    private ConcurrentHashMap<String, ArrayList<Integer>> citySectorMap;
    private ConcurrentHashMap<String, ArrayList<Integer>> stateSectorMap;
    private ConcurrentHashMap<String, ArrayList<Integer>> regionSetorMap;
    private ConcurrentHashMap<Integer, List<SectorWareHousePriorityAccessBean>> wareHousePriorityBySector;

    private OrderService orderService;
    private ProductService productService;
    private ProductInventoryService productInventoryService;
    private ArrayList<Integer> gloabalSectorIds;
    private List<SectorAccessBean> sectorData;

    private Boolean enableGlobalSectorFallback;

    private static final Logger LOGGER = LoggerFactory.getLogger(HoldOrderHelperImplementation.class);

    @Autowired
    public HoldOrderHelperImplementation(OrderService orderService, ProductService productService, ProductInventoryService productInventoryService) {
        this.orderService = orderService;
        this.productService = productService;
        this.productInventoryService = productInventoryService;
        this.gloabalSectorIds = new ArrayList<>();
    }

    @Lazy
    @Autowired
    @Qualifier(value = "CitySectorMap")
    public void setCitySectorMap(ConcurrentHashMap<String, ArrayList<Integer>> citySectorMap) {
        this.citySectorMap = citySectorMap;
    }

    @Lazy
    @Autowired
    @Qualifier(value = "StateSectorMap")
    public void setStateSectorMap(ConcurrentHashMap<String, ArrayList<Integer>> stateSectorMap) {
        this.stateSectorMap = stateSectorMap;
    }

    @Lazy
    @Autowired
    @Qualifier(value = "RegionSectorMap")
    public void setRegionSetorMap(ConcurrentHashMap<String, ArrayList<Integer>> regionSetorMap) {
        this.regionSetorMap = regionSetorMap;
    }

    @Lazy
    @Autowired
    @Qualifier(value = "SectorDetails")
    public void setSectorData(List<SectorAccessBean> sectorData) {
        this.sectorData = sectorData;
    }

    @Autowired
    @Qualifier(value = "WareHousePriorities")
    public void setWareHousePriorityBySector(ConcurrentHashMap<Integer, List<SectorWareHousePriorityAccessBean>> wareHousePriorityBySector) {
        this.wareHousePriorityBySector = wareHousePriorityBySector;
    }

    @Autowired
    @Qualifier(value = "GLOBAL_SECTOR_FALLBACK")
    public void setEnableGlobalSectorFallback(Boolean enableGlobalSectorFallback) {
        this.enableGlobalSectorFallback = enableGlobalSectorFallback;
    }

    /**
     * This method validates the request and provides the resolution method for holding the method.
     *
     * @param productRequestBean Product items of an order
     * @return StringBuffer The resolution stratergy.
     */
    private StringBuffer validateOrder(ProductRequestBean productRequestBean) {
        if ((productRequestBean.getDeliveryLocation() != null) && (!productRequestBean.getProductDetails().isEmpty())) {
            AddressAccessBean addressAccessBean = productRequestBean.getDeliveryLocation();
            if (addressAccessBean.getCity() != null && citySectorMap.containsKey(addressAccessBean.getCity())) {
                return InventoryConstants.RESOLVE_BY_CITY;
            } else if (addressAccessBean.getState() != null && stateSectorMap.containsKey(addressAccessBean.getState())) {
                return InventoryConstants.RESOLVE_BY_STATE;
            } else if (addressAccessBean.getRegion() != null && regionSetorMap.containsKey(addressAccessBean.getRegion())) {
                return InventoryConstants.RESOLVE_BY_REGION;
            }
        }
        return InventoryConstants.RESOLUTION_FAILED;
    }

    public OrderDTO processOrderRequest(ProductRequestBean productRequestBean) {
        OrderDTO orderDTO = new OrderDTO();
        StringBuffer resolveInventoryUsing = validateOrder(productRequestBean);
        if (!resolveInventoryUsing.equals(InventoryConstants.RESOLUTION_FAILED)) {
            try {
                LOGGER.info("Holding new order : {}", productRequestBean);
                ArrayList<Integer> processedSectors = new ArrayList<>();
                orderDTO = processHold(productRequestBean, resolveInventoryUsing,processedSectors);
                if(orderDTO.getOrderStatus().equals(InventoryConstants.GLOBAL_SECTOR_FALLBACK.toString()) && enableGlobalSectorFallback){
                    orderDTO = processHold(productRequestBean,InventoryConstants.GLOBAL_SECTOR_FALLBACK,processedSectors);
                }
            } catch (Exception e) {
                orderDTO.setOrderStatus(InventoryConstants.ORDER_HOLD_FAIL);
                LOGGER.error("Error holding order : {} ", productRequestBean, e);
            }
        } else {
            orderDTO.setOrderId(-1L);
            orderDTO.setOrderStatus("Invalid Details of product or delivery location");
        }

        LOGGER.info("New Hold order status : {}", orderDTO);
        return orderDTO;
    }

    /**
     * This method will perform the hold inventory operation by using resolution stratergy and returns Order status.
     *
     * @param productRequestBean    Product itesm of order.
     * @param resolveInventoryUsing Inventory resolution stratergy
     * @return OrderDTO order info of hold oder request
     * @throws Exception If hold order fails due to unknown cause
     */
    @Transactional
    private OrderDTO processHold(ProductRequestBean productRequestBean, StringBuffer resolveInventoryUsing,ArrayList<Integer> processedSectors) throws Exception {

        OrderDTO orderDTO = new OrderDTO();
        OrderAccessBean orderAccessBean = new OrderAccessBean();
        orderAccessBean.setNumberOfItems(0);
        ArrayList<Integer> eligibleSectorsList;

        if (resolveInventoryUsing.equals(InventoryConstants.RESOLVE_BY_CITY)) {
            eligibleSectorsList = citySectorMap.get(productRequestBean.getDeliveryLocation().getCity());
        } else if (resolveInventoryUsing.equals(InventoryConstants.RESOLVE_BY_STATE)) {
            eligibleSectorsList = stateSectorMap.get(productRequestBean.getDeliveryLocation().getState());
        } else if (resolveInventoryUsing.equals(InventoryConstants.RESOLVE_BY_REGION)) {
            eligibleSectorsList = regionSetorMap.get(productRequestBean.getDeliveryLocation().getRegion());
        } else if (enableGlobalSectorFallback && resolveInventoryUsing.equals(InventoryConstants.GLOBAL_SECTOR_FALLBACK)) {
            LOGGER.info("Holding Inventory using GlobalSector Priority");
            eligibleSectorsList = new ArrayList<>(gloabalSectorIds);
            eligibleSectorsList.removeAll(processedSectors);
        }
        else {
            orderDTO.setOrderStatus(InventoryConstants.RESOLUTION_FAILED.toString());
            return orderDTO;
        }

        for (Integer sectorId : eligibleSectorsList) {
            ArrayList<ProductDTO> productsToBeRemoved = new ArrayList<>();

            productRequestBean.getProductDetails().forEach(productDTO -> {
                productService.findByProductId(productDTO.getProductId()).ifPresent(productAccessBean -> {
                    if (productAccessBean.getAvailableQty() >= productDTO.getProductQuantity()) {

                        ArrayList<WareHouseAccessBean> wareHousesToSearchForInventory = new ArrayList<>();
                        wareHousePriorityBySector.get(sectorId).forEach(wareHouseInfo -> {
                            wareHousesToSearchForInventory.add(new WareHouseAccessBean(wareHouseInfo.getWareHouseId().getWareHouseId()));
                        });

                        LOGGER.debug("Processing sectorId: {}, obtained wareHousesToSearchForInventory: {}",sectorId,wareHousesToSearchForInventory);

                        Optional<List<ProductInventoryAccessBean>> inventoryByProductIdByWareHouseIds = productInventoryService.findInventoryByProductIdByWareHouseIds(new ProductAccessBean(productDTO.getProductId()), wareHousesToSearchForInventory);
                        if (inventoryByProductIdByWareHouseIds.isPresent()) {
                            for (ProductInventoryAccessBean productInventoryAccessBean : inventoryByProductIdByWareHouseIds.get()) {

                                OrderItemsAccessBean orderItemsAccessBean = new OrderItemsAccessBean();
                                orderItemsAccessBean.setProductId(productInventoryAccessBean.getProductId());
                                orderItemsAccessBean.setWareHouseId(productInventoryAccessBean.getWareHouseId());
                                orderItemsAccessBean.setOrderItemStatus(InventoryConstants.ORDER_PENDING);
                                orderItemsAccessBean.setInventoryAllocationStatus(InventoryConstants.HOLD_INVENTORY);
                                orderAccessBean.addOrderItem(orderItemsAccessBean);

                                if (productInventoryAccessBean.getAvailableQTY() >= productDTO.getProductQuantity()) {
                                    productInventoryAccessBean.setAvailableQTY(productInventoryAccessBean.getAvailableQTY() - productDTO.getProductQuantity());
                                    orderItemsAccessBean.setProductQuantity(productDTO.getProductQuantity());

                                    productsToBeRemoved.add(productDTO);
                                    orderAccessBean.setNumberOfItems(orderAccessBean.getNumberOfItems() + orderItemsAccessBean.getProductQuantity());
                                    productAccessBean.setAvailableQty(productAccessBean.getAvailableQty() - orderItemsAccessBean.getProductQuantity());

                                    //UPDATING INVENTORY
                                    productService.saveOrUpdateProduct(productAccessBean);
                                    productInventoryService.saveProductInventoryRecord(productInventoryAccessBean);
                                    break;
                                } else {
                                    productDTO.setProductQuantity(productDTO.getProductQuantity() - productInventoryAccessBean.getAvailableQTY());
                                    orderItemsAccessBean.setProductQuantity(productInventoryAccessBean.getAvailableQTY());
                                    productInventoryAccessBean.setAvailableQTY(0);
                                    orderAccessBean.setNumberOfItems(orderAccessBean.getNumberOfItems() + orderItemsAccessBean.getProductQuantity());
                                    productAccessBean.setAvailableQty(productAccessBean.getAvailableQty() - orderItemsAccessBean.getProductQuantity());

                                    //UPDATING INVENTORY
                                    productService.saveOrUpdateProduct(productAccessBean);
                                    productInventoryService.saveProductInventoryRecord(productInventoryAccessBean);
                                }

                            }
                        }

                    } else {
                        orderDTO.addOutOfStockProduct(productDTO.getProductId());
                        productsToBeRemoved.add(productDTO);
                    }
                });
            });

            processedSectors.add(sectorId);

            productsToBeRemoved.forEach(productDTO -> {
                productRequestBean.getProductDetails().remove(productDTO);
            });

            if (productRequestBean.getProductDetails().isEmpty()) {
                break;
            }
        }


        if (!orderAccessBean.getOrderItems().isEmpty()) {
            orderAccessBean.setOrderStatus(InventoryConstants.ORDER_PENDING);
            orderAccessBean.setDateOfCreation(Instant.now());
            orderAccessBean.setLastUpdated(Instant.now());
            orderAccessBean.setInventoryAllocationStatus(InventoryConstants.HOLD_INVENTORY);

            Set<OrderItemsAccessBean> orderItems = orderAccessBean.getOrderItems();
            orderAccessBean.setOrderItems(null);

            //SAVING ORDER FOR 1ST TIME.
            orderDTO.setOrderId(orderService.saveOrUpdateOrder(orderAccessBean).getOrderId());

            orderAccessBean.setOrderId(orderDTO.getOrderId());
            orderAccessBean.setOrderItems(orderItems);

            orderItems.forEach(orderItem -> {
                orderItem.setOrderId(orderAccessBean);
                orderItem.setLastUpdated(Instant.now());
            });

            //SAVING ORDER WITH ORDER_ITEMS
            orderService.saveOrUpdateOrder(orderAccessBean);

            if (productRequestBean.getProductDetails().isEmpty()) {
                orderDTO.setOrderStatus(InventoryConstants.ORDER_PENDING);
            } else {
                orderDTO.setOrderStatus(InventoryConstants.GLOBAL_SECTOR_FALLBACK.toString());
            }
        } else if (orderAccessBean.getOrderItems().isEmpty() && !productRequestBean.getProductDetails().isEmpty()) {
            orderDTO.setOrderStatus(InventoryConstants.GLOBAL_SECTOR_FALLBACK.toString());
        } else {
            orderDTO.setOrderStatus(InventoryConstants.ORDER_ITEMS_OUT_OF_STOCK);
        }

        return orderDTO;
    }

    @PostConstruct
    public void setupGlobalSectorIds(){
        sectorData.forEach(sectorAccessBean -> {
            gloabalSectorIds.add(sectorAccessBean.getSectorId());
        });
    }

}
