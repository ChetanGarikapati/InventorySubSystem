package com.inventory.components.impl;

import com.inventory.components.CancelOrderHelper;
import com.inventory.components.ConfirmOrderHelper;
import com.inventory.components.HoldOrderHelper;
import com.inventory.components.PlaceOrderFacade;
import com.inventory.dto.OrderDTO;
import com.inventory.dto.ProductRequestBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This is the base implementation for PlaceOrderFacade providing operations for Inventory operation management.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 9-Nov-2018
 */
@Component
public class PlaceOrderFacadeImplementation implements PlaceOrderFacade {

    private HoldOrderHelper holdOrderHelper;
    private ConfirmOrderHelper confirmOrderHelper;
    private CancelOrderHelper cancelOrderHelper;

    @Autowired
    public PlaceOrderFacadeImplementation(HoldOrderHelper holdOrderHelper, ConfirmOrderHelper confirmOrderHelper, CancelOrderHelper cancelOrderHelper) {
        this.holdOrderHelper = holdOrderHelper;
        this.confirmOrderHelper = confirmOrderHelper;
        this.cancelOrderHelper = cancelOrderHelper;
    }

    @Override
    public OrderDTO holdOrder(ProductRequestBean productRequestBean) {
        return holdOrderHelper.processOrderRequest(productRequestBean);
    }

    @Override
    public OrderDTO confirmOrder(OrderDTO orderDTO) {
        return confirmOrderHelper.processOrderRequest(orderDTO);
    }

    @Override
    public OrderDTO cancelOrder(OrderDTO orderDTO) {
        return cancelOrderHelper.processOrderRequest(orderDTO);
    }
}
