package com.inventory.components.impl;

import com.inventory.components.CancelOrderHelper;
import com.inventory.constants.InventoryConstants;
import com.inventory.dto.OrderDTO;
import com.inventory.entities.OrderAccessBean;
import com.inventory.services.OrderService;
import com.inventory.services.ProductInventoryService;
import com.inventory.services.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;

/**
 * @author Chetan Garikapati
 * <br>
 * Created on : 10-Nov-2018
 */
@Component(value = "BaseCancelOrderHelperImplementation")
public class CancelOrderHelperImplementation implements CancelOrderHelper {

    private ProductService productService;
    private ProductInventoryService productInventoryService;
    private OrderService orderService;

    private static final Logger LOGGER = LoggerFactory.getLogger(CancelOrderHelperImplementation.class);

    @Autowired
    public CancelOrderHelperImplementation(ProductService productService, ProductInventoryService productInventoryService, OrderService orderService) {
        this.productService = productService;
        this.productInventoryService = productInventoryService;
        this.orderService = orderService;
    }

    /**
     * This will validate if the order is in eligible state to be cancelled.
     *
     * @param orderDTO Should contain orderId obtained from hold inventory process
     * @return String Contains the validation status.
     */
    private String validateOrder(OrderDTO orderDTO) {
        if (orderDTO != null && orderDTO.getOrderId() != null) {
            Optional<OrderAccessBean> optionalOrderAccessBean = orderService.findOrderByOrderId(orderDTO.getOrderId());
            if (optionalOrderAccessBean.isPresent()) {
                if (optionalOrderAccessBean.get().getOrderStatus().equals(InventoryConstants.ORDER_PENDING) || optionalOrderAccessBean.get().getOrderStatus().equals(InventoryConstants.ORDER_CONFIRMED)) {
                    optionalOrderAccessBean.get().setOrderStatus(InventoryConstants.ORDER_CANCELLED);
                    optionalOrderAccessBean.get().setInventoryAllocationStatus(InventoryConstants.DEALLOCATE_INVENTORY);
                    optionalOrderAccessBean.get().setLastUpdated(Instant.now());
                    optionalOrderAccessBean.get().getOrderItems().forEach(orderItem -> {
                        orderItem.setLastUpdated(Instant.now());
                        orderItem.setInventoryAllocationStatus(InventoryConstants.DEALLOCATE_INVENTORY);
                        orderItem.setOrderItemStatus(InventoryConstants.ORDER_CANCELLED);
                    });
                    return InventoryConstants.ORDER_CANCELLED;
                } else {
                    LOGGER.info("Order : {} cannot be cancelled not in PENDING or CONFIRMED state, current status : {} ", optionalOrderAccessBean.get().getOrderId(), optionalOrderAccessBean.get().getOrderStatus());
                    return InventoryConstants.ORDER_NOT_IN_CANCELLABLE_STATE;
                }
            }
        }
        return InventoryConstants.ORDER_DETAILS_INVALID;
    }

    @Override
    @Transactional
    public OrderDTO processOrderRequest(OrderDTO orderDTO) {
        String validationResult = validateOrder(orderDTO);
        if (validationResult.equals(InventoryConstants.ORDER_CANCELLED)) {
            if (restockInventory(orderDTO)) {
                orderDTO.setOrderStatus(InventoryConstants.ORDER_CANCELLED);
                LOGGER.info("Cancel order successful for : {}", orderDTO);
            } else {
                orderDTO.setOrderStatus(InventoryConstants.ORDER_CANCELLATION_FAILED);
                LOGGER.warn("Restocking for order : {} failed", orderDTO);
            }
        } else {
            orderDTO.setOrderStatus(validationResult);
            LOGGER.info("Failed validation for cancel order request : {} ", orderDTO);
        }
        return orderDTO;
    }

    /**
     * THis will restock the inventory of the cancelled order.
     *
     * @param orderDTO Should contain orderId obtained from hold inventory process
     * @return boolean If restock operation was successful or not
     */
    private boolean restockInventory(OrderDTO orderDTO) {
        Optional<OrderAccessBean> optionalOrderAccessBean = orderService.findOrderByOrderId(orderDTO.getOrderId());
        if (optionalOrderAccessBean.isPresent()) {
            optionalOrderAccessBean.get().getOrderItems().forEach(orderItem -> {
                productInventoryService.findInventoryByProductIdAndWareHouseId(orderItem.getProductId(), orderItem.getWareHouseId()).ifPresent(productInventoryAccessBean -> {
                    productInventoryAccessBean.setAvailableQTY(productInventoryAccessBean.getAvailableQTY() + orderItem.getProductQuantity());
                    productService.findByProductId(orderItem.getProductId().getProductId()).ifPresent(productAccessBean -> productAccessBean.setAvailableQty(productAccessBean.getAvailableQty() + orderItem.getProductQuantity()));
                });
            });

            return InventoryConstants.OPERATIONSUCCESSFUL;
        }

        return InventoryConstants.OPERATIONFAILED;
    }
}
