package com.inventory.components.impl;

import com.inventory.components.ConfirmOrderHelper;
import com.inventory.constants.InventoryConstants;
import com.inventory.dto.OrderDTO;
import com.inventory.entities.OrderAccessBean;
import com.inventory.services.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;

/**
 * This is the base implementation class for ConfirmOrderHelper for confirming order.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 10-Nov-2018
 */
@Component(value = "BaseConfirmOrderHelperImplementation")
public class ConfirmOrderHelperImplementation implements ConfirmOrderHelper {

    private OrderService orderService;
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfirmOrderHelperImplementation.class);

    @Autowired
    public ConfirmOrderHelperImplementation(OrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    @Transactional
    public OrderDTO processOrderRequest(OrderDTO orderDTO) {
        if (orderDTO != null && orderDTO.getOrderId() != null) {
            Optional<OrderAccessBean> optionalOrderAccessBean = orderService.findOrderByOrderId(orderDTO.getOrderId());
            if (optionalOrderAccessBean.isPresent()) {
                if (optionalOrderAccessBean.get().getOrderStatus().equals(InventoryConstants.ORDER_PENDING)) {
                    optionalOrderAccessBean.get().setOrderStatus(InventoryConstants.ORDER_CONFIRMED);
                    optionalOrderAccessBean.get().setLastUpdated(Instant.now());
                    optionalOrderAccessBean.get().setInventoryAllocationStatus(InventoryConstants.ALLOCATE_INVENTORY);
                    optionalOrderAccessBean.get().getOrderItems().forEach(orderItem -> {
                        orderItem.setOrderItemStatus(InventoryConstants.ORDER_CONFIRMED);
                        orderItem.setInventoryAllocationStatus(InventoryConstants.ALLOCATE_INVENTORY);
                        orderItem.setLastUpdated(Instant.now());
                    });

                    orderDTO.setOrderStatus(InventoryConstants.ORDER_CONFIRMED);
                    LOGGER.info("Confirmation order successful for order : {}", orderDTO);

                } else {
                    orderDTO.setOrderStatus(InventoryConstants.ORDER_NOT_IN_PENDING_STATE);
                    LOGGER.info("Order : {} cannot be confirmed not in PENDING state, current status : {} ", optionalOrderAccessBean.get().getOrderId(), optionalOrderAccessBean.get().getOrderStatus());
                }
            } else {
                orderDTO.setOrderStatus(InventoryConstants.ORDER_DETAILS_INVALID);
            }
        } else {
            orderDTO.setOrderStatus(InventoryConstants.ORDER_DETAILS_INVALID);
        }
        return orderDTO;
    }

}
