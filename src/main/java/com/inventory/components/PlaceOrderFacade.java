package com.inventory.components;

import com.inventory.dto.OrderDTO;
import com.inventory.dto.ProductRequestBean;

/**
 * This is the base template for class providing functionality of providing order fullfillment related tasks.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 9/11/18-Nov-2018
 */
public interface PlaceOrderFacade {

    /**
     * This method will perform the holdInventory operation for the inventory
     * to be allocated and kept before confirming order by payment.
     *
     * @param productRequestBean Product items of the order
     * @return OrderDTO Contains info for current request.
     */
    public OrderDTO holdOrder(ProductRequestBean productRequestBean);

    /**
     * This will actually confirm the order and performs the inventory allocation.
     *
     * @param orderDTO Should contain orderId obtains from hold Inventory.
     * @return OrderDTO Contains info for current request.
     */
    public OrderDTO confirmOrder(OrderDTO orderDTO);

    /**
     * This will cancel the order and restocks the inventory must be called only if order is in "PENDING" or "CONFIRMED" state.
     *
     * @param orderDTO Should contain orderId obtains from hold Inventory or confirm order.
     * @return OrderDTO Contains info for current request.
     */
    public OrderDTO cancelOrder(OrderDTO orderDTO);
}
