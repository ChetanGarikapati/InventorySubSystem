package com.inventory.components;

import com.inventory.dto.OrderDTO;
import org.springframework.transaction.annotation.Transactional;

/**
 * This is the base template for class provide cancel order operations.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 10-Nov-2018
 */
public interface CancelOrderHelper {

    /**
     * This method will cancel the order in "PENDING" or "CONFIRMED" state.
     * @param orderDTO Should contain orderId obtained from hold inventory process
     * @return OrderDTO Contains info on current request
     */
    @Transactional
    public OrderDTO processOrderRequest(OrderDTO orderDTO);
}
