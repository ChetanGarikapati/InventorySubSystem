package com.inventory.components;

import com.inventory.dto.OrderDTO;
import org.springframework.transaction.annotation.Transactional;

/**
 * This is the base template for class providing Order confirmation functionality.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 10/11/18-Nov-2018
 */
public interface ConfirmOrderHelper {

    /**
     * This method will perform order confirmation process to start fulfillment process.
     *
     * @param orderDTO Should contain orderId obtained from hold inventory process
     * @return OrderDTO Contains info on current request
     */
    @Transactional
    public OrderDTO processOrderRequest(OrderDTO orderDTO);
}
