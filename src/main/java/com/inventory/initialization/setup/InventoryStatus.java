package com.inventory.initialization.setup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

/**
 * Information class that provides status of the application.
 *
 * @author Chetan Garikapati
 * <br>
 * Created on : 20-Oct-2018
 */
@Component
public class InventoryStatus {

    private AnnotationConfigWebApplicationContext inventoryApplicationContext;

    private Boolean applicationRunning;
    private String containerId;

    public InventoryStatus(@Autowired(required = false) AnnotationConfigWebApplicationContext inventoryApplicationContext) {
        this.inventoryApplicationContext = inventoryApplicationContext;
        if (inventoryApplicationContext != null) {
            setContainerId();
            setApplicationRunning();
        }
    }

    public String getContainerId() {
        return containerId;
    }

    private void setContainerId() {
        this.containerId = inventoryApplicationContext.getId();
    }

    public Boolean getApplicationRunning() {
        return applicationRunning;
    }

    private void setApplicationRunning() {
        this.applicationRunning = inventoryApplicationContext.isActive();
    }

    @Override
    public String toString() {
        return "InventoryStatus [ContainerId()=" + getContainerId() + ", ApplicationRunning()="
                + getApplicationRunning() + "]";
    }

}
