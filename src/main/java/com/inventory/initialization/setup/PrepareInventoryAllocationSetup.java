package com.inventory.initialization.setup;

import com.inventory.entities.SectorAccessBean;
import com.inventory.entities.SectorWareHousePriorityAccessBean;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public interface PrepareInventoryAllocationSetup {

    public ConcurrentMap<String, ArrayList<Integer>> getCitySectorMap();

    public ConcurrentMap<String, ArrayList<Integer>> getStateSectorMap();

    public ConcurrentMap<String, ArrayList<Integer>> getRegionSetorMap();

    public List<SectorAccessBean> getSectorData();

    public ConcurrentHashMap<Integer, List<SectorWareHousePriorityAccessBean>> getWareHousePriorityBySector();

    public void prepareRegistryForInventoryAllocation();
}
