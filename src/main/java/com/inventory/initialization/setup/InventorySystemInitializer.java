package com.inventory.initialization.setup;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import org.slf4j.LoggerFactory;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

/**
 * <p>
 * This is base class for starting up the InventorySystem and is processed at
 * Server startup. </br>
 * This class setups and starts Spring Container and provides context URI of
 * spring dispatcher servlet and logging levels along with base path for JPA
 * Properties.
 *
 * <p>
 * Do not modify any configurations to add custom configurations add them in
 * <strong>InventoryConfigurations</strong> class.
 *
 * @author Chetan Garikapati <br>
 * Created on : 20-Oct-2018
 */
public class InventorySystemInitializer implements WebApplicationInitializer {

    private static final String JPAPROPERTIES_PATH = "META-INF/JPAProperties.properties";
    private static final String INVENTORYCONFIGURATION_PATH = "META-INF/InventoryConfigurations.properties";

    @Override
    public void onStartup(ServletContext servletContext) {

        AnnotationConfigWebApplicationContext inventoryApplicationContext = new AnnotationConfigWebApplicationContext();
        inventoryApplicationContext.register(InventoryConfigurations.class);

        servletContext.addListener(new ContextLoaderListener(inventoryApplicationContext));
        ServletRegistration.Dynamic dispatcherServlet = servletContext.addServlet("Dispatcher Servlet",
                new DispatcherServlet(inventoryApplicationContext));
        dispatcherServlet.addMapping("/inventory-services/*");
        dispatcherServlet.setLoadOnStartup(1);

        servletContext.setInitParameter("JPAPropertiesPath", JPAPROPERTIES_PATH);
        servletContext.setInitParameter("InventoryConfigurationsPath", INVENTORYCONFIGURATION_PATH);

        setLoggingLevels();
    }

    /**
     * This method provides logging levels change as required.
     */
    private void setLoggingLevels() {
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        Logger logger = loggerContext.getLogger("org");
        logger.setLevel(Level.INFO);
    }

}
