package com.inventory.initialization.setup.impl;

import com.inventory.entities.SectorAccessBean;
import com.inventory.entities.SectorWareHousePriorityAccessBean;
import com.inventory.initialization.setup.PrepareInventoryAllocationSetup;
import com.inventory.services.SectorService;
import com.inventory.services.SectorWareHousePriorityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Configuration(value = "InventoryAllocationSetup")
public class PrepareInventoryAllocationSetupImplementation implements PrepareInventoryAllocationSetup {

    private static final Logger LOGGER = LoggerFactory.getLogger(PrepareInventoryAllocationSetupImplementation.class);
    private ConcurrentHashMap<String, ArrayList<Integer>> citySectorMap;
    private ConcurrentHashMap<String, ArrayList<Integer>> stateSectorMap;
    private ConcurrentHashMap<String, ArrayList<Integer>> regionSetorMap;
    private ConcurrentHashMap<Integer, List<SectorWareHousePriorityAccessBean>> wareHousePriorityBySector;

    private List<SectorAccessBean> sectorData = new ArrayList<>();

    private SectorService sectorService;
    private SectorWareHousePriorityService sectorWareHousePriorityService;

    @Autowired
    public PrepareInventoryAllocationSetupImplementation(SectorService sectorService, SectorWareHousePriorityService sectorWareHousePriorityService) {
        this.citySectorMap = new ConcurrentHashMap<>();
        this.stateSectorMap = new ConcurrentHashMap<>();
        this.regionSetorMap = new ConcurrentHashMap<>();
        this.wareHousePriorityBySector = new ConcurrentHashMap<>();

        this.sectorService = sectorService;
        this.sectorWareHousePriorityService = sectorWareHousePriorityService;
    }

    @Bean(name = "CitySectorMap")
    public ConcurrentMap<String, ArrayList<Integer>> getCitySectorMap() {
        return citySectorMap;
    }

    @Bean(name = "StateSectorMap")
    public ConcurrentMap<String, ArrayList<Integer>> getStateSectorMap() {
        return stateSectorMap;
    }

    @Bean(name = "RegionSectorMap")
    public ConcurrentMap<String, ArrayList<Integer>> getRegionSetorMap() {
        return regionSetorMap;
    }

    @Bean(name = "SectorDetails")
    public List<SectorAccessBean> getSectorData() {
        return sectorData;
    }

    @Bean(name = "WareHousePriorities")
    public ConcurrentHashMap<Integer, List<SectorWareHousePriorityAccessBean>> getWareHousePriorityBySector() {
        return wareHousePriorityBySector;
    }

    public void prepareRegistryForInventoryAllocation() {
        Optional<List<SectorAccessBean>> optionalSectorsInfo = sectorService.findAllSectorsSortedByPriority(false);

        if (optionalSectorsInfo.isPresent()) {
            LOGGER.info("Preparing Sector Resolution Maps");
            citySectorMap.clear();
            stateSectorMap.clear();
            regionSetorMap.clear();
            wareHousePriorityBySector.clear();
            this.sectorData = optionalSectorsInfo.get();
            sectorData.forEach(sector -> {

                if (!citySectorMap.containsKey(sector.getSectorIdentificationArea().getCity())) {
                    citySectorMap.put(sector.getSectorIdentificationArea().getCity(), new ArrayList<Integer>());
                }
                citySectorMap.get(sector.getSectorIdentificationArea().getCity()).add(sector.getSectorId());

                if (!stateSectorMap.containsKey(sector.getSectorIdentificationArea().getState())) {
                    stateSectorMap.put(sector.getSectorIdentificationArea().getState(), new ArrayList<Integer>());
                }
                stateSectorMap.get(sector.getSectorIdentificationArea().getState()).add(sector.getSectorId());

                if (!regionSetorMap.containsKey(sector.getSectorIdentificationArea().getRegion())) {
                    regionSetorMap.put(sector.getSectorIdentificationArea().getRegion(), new ArrayList<Integer>());
                }
                regionSetorMap.get(sector.getSectorIdentificationArea().getRegion()).add(sector.getSectorId());

                sectorWareHousePriorityService.findWareHousePriorityBySectorId(sector.getSectorId(), false)
                        .ifPresent(wareHousesData -> {
                            wareHousePriorityBySector.put(sector.getSectorId(), wareHousesData);
                        });
            });
        }
    }

    @PostConstruct
    public void setupMappings() {
        prepareRegistryForInventoryAllocation();
        LOGGER.info("CitySectorMap created with size : {}", citySectorMap.size());
        LOGGER.debug("CitySectorMap created with size : {} , data : {}", citySectorMap.size(),citySectorMap);

        LOGGER.info("StateSectorMap created with size : {}", stateSectorMap.size());
        LOGGER.debug("StateSectorMap created with size : {} , data : {}", stateSectorMap.size(),stateSectorMap);

        LOGGER.info("RegionSectorMap created with size : {}", regionSetorMap.size());
        LOGGER.debug("RegionSectorMap created with size : {}, data : {}", regionSetorMap.size(),regionSetorMap);

        LOGGER.debug("WareHousePriorities Map created with size {}", wareHousePriorityBySector.size());
        LOGGER.debug("WareHousePriorities Map created with size {}, data : {}", wareHousePriorityBySector.size(),wareHousePriorityBySector);
    }
}
